﻿namespace PatientHub.Framework.UWP.UITest
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UITest.Input;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UITesting.WindowsRuntimeControls;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;

    using PatientHub.Framework.FunctionalTesting.Test.DataAccess;
    using System.Text.RegularExpressions;
    using RelevantCodes.ExtentReports;
    using System.Diagnostics;
    using System.IO;

    [CodedUITest(CodedUITestType.WindowsStore)]
    public class UwpAppBvt : UwpAppBase
    {
        public UwpAppBvt()
        {
        }

        [ClassInitialize()]
        public static void MyClassInitialize(TestContext testContext)
        {
            //Start Extent Report
            SetupReporting("UWP");
        }

        [ClassCleanup()]
        public static void MyClassCleanUp()
        {
            //Close Extent Report
            CloseReporting();
        }
        
        [TestMethod]
        public void AddPeakFlow_PostiveScenario()
        {
            int testCaseId = 7416;
            extentTest = extentReports.StartTest(testCaseId + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name);

            TestPatient testPatient = TestDataHelper.GetPatientForTestCase(this.SqlitePlatform, testCaseId);
            TestUser testUser = TestDataHelper.GetUserForTestCase(this.SqlitePlatform, testCaseId, testPatient.PracticeID);
            TestEncounter testEncounter = TestDataHelper.GetEncounterForTestCase(this.SqlitePlatform, testCaseId, testPatient.RegisteredGP);
            TestPeakFlow testPeakFlowEntry = TestDataHelper.GetPeakFlowForEncounter(this.SqlitePlatform, testEncounter.ID, testPatient.RegisteredGP);

            Login(testUser);
            SearchForPatient(testPatient.FamilyName + " " + testPatient.GivenName);
            SelectPatient(testPatient);
            CheckPatientDetailsOnPatientBanner(testPatient);

            // Add a new encounter
            UWPApp.UwpAppPatientHub.MainWindow.PatientHubSummary.AddNewEncounterButton.WaitForControlReady(this.WaitForReadyTimeout);
            Mouse.Click(UWPApp.UwpAppPatientHub.MainWindow.PatientHubSummary.AddNewEncounterButton);
            DateTime dateTimeEncounterAdded = DateTime.Now;
            SetEncounterType(testEncounter.EncounterType);

            // ADD Peak Flow ENTRY
            // Add To Encounter: Search for Peak Flow and select the quick entry form
            SearchForQuickEntryForm(testPeakFlowEntry.QuickListSelection);

            // Enter details of Peak Flow entry and save
            EnterPeakFlowEntryDetails(testPeakFlowEntry);
            SavePeakFlowEntryDetails();

            // Check the entry on Current Encounter, then save encounter
            CheckPeakFlowEntryOnCurrentEncounter(testEncounter, testPeakFlowEntry);
            SaveEncounterDetails(testEncounter);

            // Check the entries on Patient Hub Last Encounter
            CheckLastEncounter(testUser, testEncounter, dateTimeEncounterAdded);
            CheckPeakFlowEntryOnLastEncounter(testPeakFlowEntry);

            // Check the entries on Patient Hub Examinations and Lifestyle
            CheckPeakFlowEntryOnPatientHub(testEncounter, testPeakFlowEntry, testUser, dateTimeEncounterAdded);

            //// Check the entries on the Patient section page for Examinations and Lifestyle
            //Mouse.Click(WpfApp.WpfAppPatientHub.MainWindow.PageRoot.PatientSummaryHub.LifestyleAndExaminationsHubSection.LifestyleAndExaminationsHeader);
            //this.CheckPeakFlowEntryOnPatientSection(testEncounter, testPeakFlowEntry, testUser, dateTimeEncounterAdded);

            //// Check the entry on the Entry Details page
            //this.CheckPeakFlowEntryOnEntryDetails(testPeakFlowEntry, testUser, dateTimeEncounterAdded);
            //Mouse.Click(WpfApp.WpfAppCommon.MainWindow.PageRoot.BackButton);
        }

        [TestMethod]
        public void AddPeakFlow_NegativeScenario()
        {
            int testCaseId = 7416;
            extentTest = extentReports.StartTest(testCaseId + " - " + System.Reflection.MethodBase.GetCurrentMethod().Name);

            TestPatient testPatient = TestDataHelper.GetPatientForTestCase(this.SqlitePlatform, testCaseId);
            TestUser testUser = TestDataHelper.GetUserForTestCase(this.SqlitePlatform, testCaseId, testPatient.PracticeID);
            TestEncounter testEncounter = TestDataHelper.GetEncounterForTestCase(this.SqlitePlatform, testCaseId, testPatient.RegisteredGP);
            TestPeakFlow testPeakFlowEntry = TestDataHelper.GetPeakFlowForEncounter(this.SqlitePlatform, testEncounter.ID, testPatient.RegisteredGP);

            Login(testUser);
            SearchForPatient(testPatient.FamilyName + " " + testPatient.GivenName);
            SelectPatient(testPatient);
            CheckPatientDetailsOnPatientBanner(testPatient);

            // Add a new encounter
            UWPApp.UwpAppPatientHub.MainWindow.PatientHubSummary.AddNewEncounterButton.WaitForControlReady(this.WaitForReadyTimeout);
            Mouse.Click(UWPApp.UwpAppPatientHub.MainWindow.PatientHubSummary.AddNewEncounterButton);
            DateTime dateTimeEncounterAdded = DateTime.Now;
            SetEncounterType(testEncounter.EncounterType);

            // ADD Peak Flow ENTRY
            // Add To Encounter: Search for Peak Flow and select the quick entry form
            SearchForQuickEntryForm(testPeakFlowEntry.QuickListSelection);

            // Enter details of Peak Flow entry and save
            EnterPeakFlowEntryDetails(testPeakFlowEntry);
            SavePeakFlowEntryDetails();

            // Check the entry on Current Encounter, then save encounter
            CheckPeakFlowEntryOnCurrentEncounter(testEncounter, testPeakFlowEntry);
            SaveEncounterDetails(testEncounter);

            // Check the entries on Patient Hub Last Encounter
            CheckLastEncounter(testUser, testEncounter, dateTimeEncounterAdded);
            CheckPeakFlowEntryOnLastEncounter(testPeakFlowEntry);

            // Check the entries on Patient Hub Examinations and Lifestyle
            CheckPeakFlowEntryOnPatientHub(testEncounter, testPeakFlowEntry, testUser, dateTimeEncounterAdded);

            //// Check the entries on the Patient section page for Examinations and Lifestyle
            //Mouse.Click(WpfApp.WpfAppPatientHub.MainWindow.PageRoot.PatientSummaryHub.LifestyleAndExaminationsHubSection.LifestyleAndExaminationsHeader);
            //this.CheckPeakFlowEntryOnPatientSection(testEncounter, testPeakFlowEntry, testUser, dateTimeEncounterAdded);

            //// Check the entry on the Entry Details page
            //this.CheckPeakFlowEntryOnEntryDetails(testPeakFlowEntry, testUser, dateTimeEncounterAdded);
            //Mouse.Click(WpfApp.WpfAppCommon.MainWindow.PageRoot.BackButton);
        }

        [TestMethod]
        public void demo()
        {
            string functionName = System.Reflection.MethodBase.GetCurrentMethod().Name;

        }

    }
}