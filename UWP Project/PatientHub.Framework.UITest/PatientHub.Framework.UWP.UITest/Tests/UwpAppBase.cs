﻿namespace PatientHub.Framework.UWP.UITest
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Microsoft.VisualStudio.TestTools.UITesting.WindowsRuntimeControls;
    using Microsoft.VisualStudio.TestTools.UITest.Input;

    using SQLite.Net;
    using SQLite.Net.Platform.Win32;
    using System.IO;
    using RelevantCodes.ExtentReports;
    using System.Reflection;

    /// <summary>
    /// Summary description for UwpAppBase class
    /// </summary>
    [CodedUITest]
    [DeploymentItem(@"PatientHub.Framework.FunctionalTesting.DataAccess\PHFTest.db")]
    [DeploymentItem(@"dlls\sqlite3.dll", "dlls")]
    [DeploymentItem(@"dlls\libeay32MD.dll", "dlls")]
    public partial class UwpAppBase
    {
        protected TestContext testContextInstance;

        protected XamlWindow AppWindow;
        protected int SearchTimeout;
        protected SQLitePlatformWin32 SqlitePlatform;
        protected int WaitForReadyTimeout;
        protected bool ContinueOnAssertFailure;
        protected List<AssertFailedException> Failures;


        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        //Use TestInitialize to run code before running each test 
        [TestInitialize()]
        public virtual void MyTestInitialize()
        {
            Failures = new List<AssertFailedException>();

            this.GetRunSettings();
            Playback.PlaybackSettings.WaitForReadyTimeout = this.WaitForReadyTimeout;
            Playback.PlaybackSettings.SearchTimeout = this.SearchTimeout;

            // Delete the local patient hub database so that we always start from a known state
            this.CloseAllInstances("PatientHub.Framework.WindowsUWP");
            this.SqlitePlatform = new SQLitePlatformWin32();
            //this.DeleteLocalDatabase();

            // Launch the app
            this.AppWindow = XamlWindow.Launch(ConfigurationManager.AppSettings["AppTileAutomationId"]);
            this.AppWindow.CloseOnPlaybackCleanup = true;

            // Maximise the app
            Keyboard.SendKeys("{UP}", ModifierKeys.Windows);
        }

        //Use TestCleanup to run code after each test has run
        [System.Runtime.ExceptionServices.HandleProcessCorruptedStateExceptions]
        [System.Security.SecurityCritical]
        [TestCleanup()]
        public virtual void MyTestCleanup()
        {
            try
            {
                // Logout (if not already logged out)
                //if (StoreApp.StoreAppCommon.VisionAnywhereWindow.CurrentUserMenuButton.Exists)
                //{
                //    Gesture.Tap(StoreApp.StoreAppCommon.VisionAnywhereWindow.CurrentUserMenuButton);
                //    this.SelectMenuItem(StoreApp.StoreAppCommon.VisionAnywhereWindow.PopupWindow.ItemMenu, "Sign out");
                //}
            }

            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(string.Format("{0}: \r\n{1}\r\n{2}\r\n{3}\r\n{4}", e.InnerException, e.Source, e.TargetSite, e.Message, e.StackTrace));
            }

            this.CloseAllInstances("Patient");
            this.ThrowAllAssertFails(this.Failures);

            //Extent Report
            CloseTestReporting();
        }

        private void GetRunSettings()
        {
            this.ContinueOnAssertFailure = Boolean.Parse(ConfigurationManager.AppSettings["ContinueOnAssertFailure"]);
            this.WaitForReadyTimeout = Int32.Parse(ConfigurationManager.AppSettings["WaitForReadyTimeout"]);
            this.SearchTimeout = Int32.Parse(ConfigurationManager.AppSettings["SearchTimeout"]);
        }
    }
}

