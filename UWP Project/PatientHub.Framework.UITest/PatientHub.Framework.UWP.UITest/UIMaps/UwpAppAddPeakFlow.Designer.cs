﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      This code was generated by coded UI test builder.
//      Version: 15.0.0.0
//
//      Changes to this file may cause incorrect behavior and will be lost if
//      the code is regenerated.
//  </auto-generated>
// ------------------------------------------------------------------------------

namespace PatientHub.Framework.UWP.UITest.UIMaps.UwpAppAddPeakFlowClasses
{
    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITest.Input;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UITesting.WindowsRuntimeControls;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = Microsoft.VisualStudio.TestTools.UITest.Input.MouseButtons;
    
    
    [GeneratedCode("Coded UITest Builder", "15.0.26621.2")]
    public partial class UwpAppAddPeakFlow
    {
        
        #region Properties
        public MainWindow MainWindow
        {
            get
            {
                if ((this.mMainWindow == null))
                {
                    this.mMainWindow = new MainWindow();
                }
                return this.mMainWindow;
            }
        }
        #endregion
        
        #region Fields
        private MainWindow mMainWindow;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "15.0.26621.2")]
    public class MainWindow : XamlWindow
    {
        
        public MainWindow()
        {
            #region Search Criteria
            this.SearchProperties[XamlControl.PropertyNames.Name] = "PatientHub.Framework.Uwp";
            this.SearchProperties[XamlControl.PropertyNames.ClassName] = "Windows.UI.Core.CoreWindow";
            this.WindowTitles.Add("PatientHub.Framework.Uwp");
            #endregion
        }
        
        #region Properties
        public ItemPane ItemPane
        {
            get
            {
                if ((this.mItemPane == null))
                {
                    this.mItemPane = new ItemPane(this);
                }
                return this.mItemPane;
            }
        }
        
        public DescriptionComboBox DescriptionComboBox
        {
            get
            {
                if ((this.mDescriptionComboBox == null))
                {
                    this.mDescriptionComboBox = new DescriptionComboBox(this);
                }
                return this.mDescriptionComboBox;
            }
        }
        
        public XamlText PeakFlowPageTitle
        {
            get
            {
                if ((this.mPeakFlowPageTitle == null))
                {
                    this.mPeakFlowPageTitle = new XamlText(this);
                    #region Search Criteria
                    this.mPeakFlowPageTitle.SearchProperties[XamlText.PropertyNames.AutomationId] = "PageTitle";
                    this.mPeakFlowPageTitle.WindowTitles.Add("PatientHub.Framework.Uwp");
                    #endregion
                }
                return this.mPeakFlowPageTitle;
            }
        }
        
        public XamlButton SaveButton
        {
            get
            {
                if ((this.mSaveButton == null))
                {
                    this.mSaveButton = new XamlButton(this);
                    #region Search Criteria
                    this.mSaveButton.SearchProperties[XamlButton.PropertyNames.AutomationId] = "SaveButton";
                    this.mSaveButton.WindowTitles.Add("PatientHub.Framework.Uwp");
                    #endregion
                }
                return this.mSaveButton;
            }
        }
        
        public XamlButton CancelButton
        {
            get
            {
                if ((this.mCancelButton == null))
                {
                    this.mCancelButton = new XamlButton(this);
                    #region Search Criteria
                    this.mCancelButton.SearchProperties[XamlButton.PropertyNames.AutomationId] = "CancelButton";
                    this.mCancelButton.WindowTitles.Add("PatientHub.Framework.Uwp");
                    #endregion
                }
                return this.mCancelButton;
            }
        }
        
        public XamlToggleButton ToggleButton
        {
            get
            {
                if ((this.mToggleButton == null))
                {
                    this.mToggleButton = new XamlToggleButton(this);
                    #region Search Criteria
                    this.mToggleButton.SearchProperties[XamlToggleButton.PropertyNames.AutomationId] = "ToggleButton";
                    this.mToggleButton.WindowTitles.Add("PatientHub.Framework.Uwp");
                    #endregion
                }
                return this.mToggleButton;
            }
        }
        
        public CancelEntryWindow CancelEntryWindow
        {
            get
            {
                if ((this.mCancelEntryWindow == null))
                {
                    this.mCancelEntryWindow = new CancelEntryWindow(this);
                }
                return this.mCancelEntryWindow;
            }
        }
        
        public ContentScrollViewer ContentScrollViewer
        {
            get
            {
                if ((this.mContentScrollViewer == null))
                {
                    this.mContentScrollViewer = new ContentScrollViewer(this);
                }
                return this.mContentScrollViewer;
            }
        }
        #endregion
        
        #region Fields
        private ItemPane mItemPane;
        
        private DescriptionComboBox mDescriptionComboBox;
        
        private XamlText mPeakFlowPageTitle;
        
        private XamlButton mSaveButton;
        
        private XamlButton mCancelButton;
        
        private XamlToggleButton mToggleButton;
        
        private CancelEntryWindow mCancelEntryWindow;
        
        private ContentScrollViewer mContentScrollViewer;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "15.0.26621.2")]
    public class ItemPane : XamlControl
    {
        
        public ItemPane(XamlControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[UITestControl.PropertyNames.ControlType] = "Pane";
            this.SearchProperties[UITestControl.PropertyNames.ClassName] = "ScrollViewer";
            this.WindowTitles.Add("PatientHub.Framework.Uwp");
            #endregion
        }
        
        #region Properties
        public XamlComboBox DescriptionComboBox
        {
            get
            {
                if ((this.mDescriptionComboBox == null))
                {
                    this.mDescriptionComboBox = new XamlComboBox(this);
                    #region Search Criteria
                    this.mDescriptionComboBox.SearchProperties[XamlComboBox.PropertyNames.AutomationId] = "DescriptionComboBox";
                    this.mDescriptionComboBox.WindowTitles.Add("PatientHub.Framework.Uwp");
                    #endregion
                }
                return this.mDescriptionComboBox;
            }
        }
        
        public XamlEdit FlowRateTextBoxEdit
        {
            get
            {
                if ((this.mFlowRateTextBoxEdit == null))
                {
                    this.mFlowRateTextBoxEdit = new XamlEdit(this);
                    #region Search Criteria
                    this.mFlowRateTextBoxEdit.SearchProperties[XamlEdit.PropertyNames.AutomationId] = "FlowRateTextBox";
                    this.mFlowRateTextBoxEdit.WindowTitles.Add("PatientHub.Framework.Uwp");
                    #endregion
                }
                return this.mFlowRateTextBoxEdit;
            }
        }
        
        public XamlEdit NotesFieldEdit
        {
            get
            {
                if ((this.mNotesFieldEdit == null))
                {
                    this.mNotesFieldEdit = new XamlEdit(this);
                    #region Search Criteria
                    this.mNotesFieldEdit.SearchProperties[XamlEdit.PropertyNames.AutomationId] = "CommentsField";
                    this.mNotesFieldEdit.WindowTitles.Add("PatientHub.Framework.Uwp");
                    #endregion
                }
                return this.mNotesFieldEdit;
            }
        }
        #endregion
        
        #region Fields
        private XamlComboBox mDescriptionComboBox;
        
        private XamlEdit mFlowRateTextBoxEdit;
        
        private XamlEdit mNotesFieldEdit;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "15.0.26621.2")]
    public class DescriptionComboBox : XamlComboBox
    {
        
        public DescriptionComboBox(XamlControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[XamlComboBox.PropertyNames.AutomationId] = "DescriptionComboBox";
            this.WindowTitles.Add("PatientHub.Framework.Uwp");
            #endregion
        }
        
        #region Properties
        public XamlListItem ListItem
        {
            get
            {
                if ((this.mListItem == null))
                {
                    this.mListItem = new XamlListItem(this);
                    #region Search Criteria
                    this.mListItem.SearchProperties[XamlListItem.PropertyNames.Name] = "60-80% of predicted peak expiratory flow rate";
                    this.mListItem.WindowTitles.Add("PatientHub.Framework.Uwp");
                    #endregion
                }
                return this.mListItem;
            }
        }
        #endregion
        
        #region Fields
        private XamlListItem mListItem;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "15.0.26621.2")]
    public class CancelEntryWindow : XamlWindow
    {
        
        public CancelEntryWindow(XamlControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[XamlControl.PropertyNames.Name] = "Cancel this entry";
            this.WindowTitles.Add("PatientHub.Framework.Uwp");
            #endregion
        }
        
        #region Properties
        public XamlButton YesButton
        {
            get
            {
                if ((this.mYesButton == null))
                {
                    this.mYesButton = new XamlButton(this);
                    #region Search Criteria
                    this.mYesButton.SearchProperties[XamlButton.PropertyNames.AutomationId] = "PrimaryButton";
                    this.mYesButton.WindowTitles.Add("PatientHub.Framework.Uwp");
                    #endregion
                }
                return this.mYesButton;
            }
        }
        
        public XamlButton NoButton
        {
            get
            {
                if ((this.mNoButton == null))
                {
                    this.mNoButton = new XamlButton(this);
                    #region Search Criteria
                    this.mNoButton.SearchProperties[XamlButton.PropertyNames.AutomationId] = "SecondaryButton";
                    this.mNoButton.WindowTitles.Add("PatientHub.Framework.Uwp");
                    #endregion
                }
                return this.mNoButton;
            }
        }
        #endregion
        
        #region Fields
        private XamlButton mYesButton;
        
        private XamlButton mNoButton;
        #endregion
    }
    
    [GeneratedCode("Coded UITest Builder", "15.0.26621.2")]
    public class ContentScrollViewer : XamlControl
    {
        
        public ContentScrollViewer(XamlControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Search Criteria
            this.SearchProperties[UITestControl.PropertyNames.ControlType] = "Pane";
            this.SearchProperties[UITestControl.PropertyNames.ClassName] = "ScrollViewer";
            this.SearchProperties["AutomationId"] = "ContentScrollViewer";
            this.WindowTitles.Add("PatientHub.Framework.Uwp");
            #endregion
        }
        
        #region Properties
        public XamlText CancelThisEntryTitleText
        {
            get
            {
                if ((this.mCancelThisEntryTitleText == null))
                {
                    this.mCancelThisEntryTitleText = new XamlText(this);
                    #region Search Criteria
                    this.mCancelThisEntryTitleText.SearchProperties[XamlText.PropertyNames.Name] = "Cancel this entry";
                    this.mCancelThisEntryTitleText.WindowTitles.Add("PatientHub.Framework.Uwp");
                    #endregion
                }
                return this.mCancelThisEntryTitleText;
            }
        }
        
        public XamlText CancelEntryCompleteText
        {
            get
            {
                if ((this.mCancelEntryCompleteText == null))
                {
                    this.mCancelEntryCompleteText = new XamlText(this);
                    #region Search Criteria
                    this.mCancelEntryCompleteText.SearchProperties[XamlText.PropertyNames.Name] = "Any text you have entered will be permanently deleted.\r\nDo you want to leave with" +
                        "out saving this entry?";
                    this.mCancelEntryCompleteText.WindowTitles.Add("PatientHub.Framework.Uwp");
                    #endregion
                }
                return this.mCancelEntryCompleteText;
            }
        }
        #endregion
        
        #region Fields
        private XamlText mCancelThisEntryTitleText;
        
        private XamlText mCancelEntryCompleteText;
        #endregion
    }
}
