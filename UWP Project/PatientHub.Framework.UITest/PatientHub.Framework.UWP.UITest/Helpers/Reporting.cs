﻿namespace PatientHub.Framework.UWP.UITest
{
    using RelevantCodes.ExtentReports;
    using System;
    using System.Runtime.InteropServices;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Drawing;
    using System.Drawing.Imaging;
    using System.Windows.Forms;

    public partial class UwpAppBase
    {
        protected static string basePath;
        protected static string folderName;
        protected static string screenshotsFolder;
        protected static string folder;
        protected static string folderpath;
        protected static string reportPath;
        protected static string logoPath;
        protected static ExtentReports extentReports;
        protected static ExtentTest extentTest;

        protected static String imagePath = "";

        public static void SetupReporting(string platform)
        {
		    string platformCode = platform.ToUpper();

            // Create folder for reports
            CreateFolder(platformCode, DateTime.Now.ToString("ddMMyyyy_HHmmss"));

            // Initialize extent report HTML
            reportPath = folderpath + Path.DirectorySeparatorChar + platformCode + "_Report.html";
            extentReports = new ExtentReports(reportPath);

            // Load Extent Configuration
            extentReports.LoadConfig(basePath + Path.DirectorySeparatorChar + "PatientHub.Framework.UWP.UITest" + Path.DirectorySeparatorChar + "extent-config.xml");

            // Copy Vision logo to report folder
            logoPath = folderpath + Path.DirectorySeparatorChar + "VisionLogo.png";
            CopyFile(basePath + Path.DirectorySeparatorChar + "Resources" + Path.DirectorySeparatorChar + "VisionLogo.png", logoPath);
            
            // Hide logo image file
            HideFile(logoPath);
            
            extentReports.Flush();
        }

        public static String CreateFolder(String platformName, String Uniqueid)
        {
            basePath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.Parent.FullName;
            folderName = "";

            // Check Reports folder: create if not created
            bool reportsFolderExists = Directory.Exists(basePath + Path.DirectorySeparatorChar + "Reports");
            if (!reportsFolderExists)
            {
                Directory.CreateDirectory(basePath + Path.DirectorySeparatorChar + "Reports");
            }

            // Check TimeStamp folder: create if not created
            folderName = basePath + Path.DirectorySeparatorChar + "Reports" + Path.DirectorySeparatorChar + Uniqueid;
            bool dateTimeStampFolderExists = Directory.Exists(folderName);
            if (!dateTimeStampFolderExists)
            {
                Directory.CreateDirectory(folderName);
            }

            // Create Platform folder
            folderName = folderName + Path.DirectorySeparatorChar + platformName;
            bool platformFolderExist = Directory.Exists(folderName);
            if (!platformFolderExist)
            {
                Directory.CreateDirectory(folderName);
            }

            // Create Screenshots folder
            screenshotsFolder = folderName + Path.DirectorySeparatorChar + "Screenshots";
            bool screenshotsFolderExist = Directory.Exists(screenshotsFolder);
            if (!screenshotsFolderExist)
            {
                Directory.CreateDirectory(screenshotsFolder);
            }

            SetFolderpath(folderName);
            Setfolder(Uniqueid);
            return folderName;
        }

        public static void SetFolderpath(String folderName)
        {
            folderpath = folderName;
            imagePath = folderpath + Path.DirectorySeparatorChar + "Screenshots" + Path.DirectorySeparatorChar;
        }

        public static void Setfolder(String foldername)
        {
            folder = foldername;
        }

        public void CloseTestReporting()
        {
            extentReports.EndTest(extentTest);
            extentReports.Flush();
        }

        public static void CloseReporting()
        {
            extentReports.Close();
            ChangeReportVersion();
            ChangeCSSProperties();
        }

        public void LogStepToReport(LogStatus status, String stepname, String Details)
        {
            try
            {
                extentTest.Log(status, stepname, Details + "<div align='right' style='float:right'><a  target='_blank' href=" + Getscreenshot() + ">Screenshot</a></div>");
            }
            catch (Exception e)
            {
                extentTest.Log(status, stepname, Details + "<div align='right' style='float:right'>Unable to take screenshot</div>");
            }
            extentReports.Flush();
        }

        public void VerifyCondition(bool conditionToCheck, String passMessage, String failMessage)
        {
            if (conditionToCheck)
            {
                AssertThat(true, passMessage, failMessage);
            }
            else
            {
                AssertThat(false, passMessage, failMessage);
            }
        }

        public void AssertThat(bool status, String passMessage, String failMessage)
        {
            if (!status)
            {
                String line = "<b> <font color='red'> Expected - " + passMessage + "<br> Actual - " + failMessage;
                extentTest.Log(LogStatus.Fail, "Verification Point", line + "<div align='right' style='float:right'><a  target='_blank' href=" + Getscreenshot() + ">Screenshot</a></div>");
            }
            else
            {
                String line = "<b> <font color='green'> Expected - " + passMessage + "<br> Actual - " + passMessage;
                extentTest.Log(LogStatus.Pass, "Verification Point", line + "<div align='right' style='float:right'><a  target='_blank' href=" + Getscreenshot() + ">Screenshot</a></div>");
            }

            extentReports.Flush();
        }

        public String Getscreenshot()
        {
            string fullpath = "";
            Rectangle bounds = Screen.GetBounds(Point.Empty);
            using (Bitmap bitmap = new Bitmap(bounds.Width, bounds.Height))
            {
                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    g.CopyFromScreen(Point.Empty, Point.Empty, bounds.Size);
                }

                fullpath = screenshotsFolder + Path.DirectorySeparatorChar + DateTime.Now.ToString("ddMMyyyy_HHmmss");

                bitmap.Save(fullpath, ImageFormat.Png);
            }

            return fullpath;
        }

        public void LogStepToReport(bool condition, String stepname, String passMessage, String failMessage)
        {
            if (condition)
            {
                LogStepToReport(LogStatus.Pass, stepname, passMessage);
            }
            else
            {
                LogStepToReport(LogStatus.Fail, stepname, failMessage);
            }
        }

        public static void ChangeReportVersion()
        {
            string text = File.ReadAllText(reportPath);
            text = text.Replace("v2.41.0", "Release 3.2");
            File.WriteAllText(reportPath, text);
        }

        public static void ChangeLogoName()
        {
            string text = File.ReadAllText(reportPath);
            text = text.Replace("<span>ExtentReports</span>", "<span>Vision Anywhere</span>");
            text = text.Replace("http://extentreports.relevantcodes.com", "https://www.visionhealth.co.uk/");
            text = text.Replace("('.logo span').html('ExtentReports')", "('.logo span').html('Vision Anywhere')");
            File.WriteAllText(reportPath, text);
        }

        public static void ChangeCSSProperties()
        {
            string text = File.ReadAllText(reportPath);
            text = text.Replace("<style>", " <style>img {display: block; margin-top: 0px; }");
            File.WriteAllText(reportPath, text);
        }

        public static void CopyFile(String fromLocation, String toLocation)
        {
            System.IO.File.Copy(fromLocation, toLocation, true);
        }

        public static void HideFile(String filePath)
        {
            File.SetAttributes(filePath, FileAttributes.Hidden);
        }

    }
}
