﻿namespace PatientHub.Framework.UWP.UITest
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Diagnostics;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Microsoft.VisualStudio.TestTools.UITest.Input;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITesting.WindowsRuntimeControls;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;

    using PatientHub.Framework.FunctionalTesting.Test.DataAccess;
    using RelevantCodes.ExtentReports;
    using System.Reflection;

    public partial class UwpAppBase
    {

        #region Screen Check

        public bool ElementsCheckOnLoginScreen()
        {
            bool loginScreenCheck = UWPApp.UwpAppLogin.VisionAnywhereWindow.ItemPane.UsernameEdit.Exists
                                  && UWPApp.UwpAppLogin.VisionAnywhereWindow.ItemPane.PasswordEdit.Exists
                                  && UWPApp.UwpAppLogin.VisionAnywhereWindow.ItemPane.SigninButton.Exists;

            return loginScreenCheck;
        }

        public bool ElementsCheckOnRoleSelectionScreen()
        {
            bool roleSelectionScreenCheck = UWPApp.UwpAppRoleSelection.VisionAnywhereWindow.ItemPane.UserRolesComboBox.Exists
                              && UWPApp.UwpAppRoleSelection.VisionAnywhereWindow.ItemPane.UserIDText.Exists
                              && UWPApp.UwpAppRoleSelection.VisionAnywhereWindow.AppointmentBookComboBox.Exists
                              && UWPApp.UwpAppRoleSelection.VisionAnywhereWindow.ItemPane.SignInButton.Exists;

            return roleSelectionScreenCheck;
        }

        public bool ElementsCheckOnClinicianHub()
        {
            bool clinicianHubScreenCheck = UWPApp.UwpAppCommon.MainWindow.GlobalSearchBox.PatientSearchTextBox.Exists
                && UWPApp.UwpAppCommon.MainWindow.CurrentUserToggleButton.Exists
                && UWPApp.UwpAppCommon.MainWindow.BackButton.Exists
                && UWPApp.UwpAppCommon.MainWindow.ItemPane.EncountersButton.Exists
                && UWPApp.UwpAppCommon.MainWindow.ItemPane.NextAppointmentButton.Exists
                && UWPApp.UwpAppCommon.MainWindow.ItemPane.AppointmentsButton.Exists
                && UWPApp.UwpAppCommon.MainWindow.ItemPane.DownloadedPatientsText.Exists
                && UWPApp.UwpAppCommon.MainWindow.ItemPane.DownloadedPatientsText.Exists;

            return clinicianHubScreenCheck;
        }

        public bool ElementsCheckOnPatientHub()
        {
            bool patientHubScreenCheck = UWPApp.UwpAppPatientHub.MainWindow.PatientHubSummary.EncountersHeaderButton.Exists
                && UWPApp.UwpAppPatientHub.MainWindow.PatientHubSummary.MedicalHistoryHeaderButton.Exists
                && UWPApp.UwpAppPatientHub.MainWindow.PatientHubSummary.AddNewEncounterButton.Exists
                && UWPApp.UwpAppPatientHub.MainWindow.PatientHubSummary.LifestyleAndExaminationHeaderButton.Exists
                && UWPApp.UwpAppPatientHub.MainWindow.PatientHubSummary.DemographicsHeaderButton.Exists
                && UWPApp.UwpAppPatientHub.MainWindow.PatientHubSummary.AllergiesHeaderButton.Exists;

            return patientHubScreenCheck;
        }

        public bool ElementsCheckOnCurrentEncounter()
        {
            bool currentEncounterScreenCheck = UWPApp.UwpAppCurrentEncounter.MainWindow.EncounterTypeComboBox.Exists
                && UWPApp.UwpAppCurrentEncounter.MainWindow.EncounterTypeComboBox.Exists;

            return currentEncounterScreenCheck;
        }

        public bool ElementsCheckOnPeakFlowScreen()
        {
            bool peakFlowScreenCheck = UWPApp.UwpAppAddPeakFlow.MainWindow.PeakFlowPageTitle.Exists
                        && UWPApp.UwpAppAddPeakFlow.MainWindow.DescriptionComboBox.Exists
                        && UWPApp.UwpAppAddPeakFlow.MainWindow.ItemPane.FlowRateTextBoxEdit.Exists
                        && UWPApp.UwpAppAddPeakFlow.MainWindow.ItemPane.NotesFieldEdit.Exists;

            return peakFlowScreenCheck;
        }

        #endregion

        #region Common

        public bool Login(TestUser testUser, TestPractice testPractice = null)
        {
            bool setAppointmentBook = false;

            //Clear Username and Password fields
            UWPApp.UwpAppLogin.VisionAnywhereWindow.ItemPane.UsernameEdit.Text = "";
            UWPApp.UwpAppLogin.VisionAnywhereWindow.ItemPane.PasswordEdit.Text = "";

            VerifyScreenElements(ElementsCheckOnLoginScreen(), "Login Screen");

            //SetHealthBoard(testHealthBoard);

            //Enter Username and Password
            EnterTextInEditField(UWPApp.UwpAppLogin.VisionAnywhereWindow.ItemPane.UsernameEdit, testUser.UserName, "Username", "Username <b>" + testUser.UserName + "</b> has been entered in the Username field");
            EnterTextInEditField(UWPApp.UwpAppLogin.VisionAnywhereWindow.ItemPane.PasswordEdit, testUser.Password, "Password", "Password has been entered in the Password field");
            ClickButton(UWPApp.UwpAppLogin.VisionAnywhereWindow.ItemPane.SigninButton, "SignIn Button", "SignIn button has been clicked");

            // Wait for the 'username' field to disappear
            DateTime endTime = DateTime.Now.AddMilliseconds(Convert.ToDouble(this.WaitForReadyTimeout));
            while (DateTime.Now < endTime)
            {
                // When the username field disappears, continue execution
                if (!UWPApp.UwpAppLogin.VisionAnywhereWindow.ItemPane.UsernameEdit.Exists)
                {
                    break;
                }

                //// If the Licence Check popup is displayed, retrun
                //if (WpfApp.WpfAppLogin.LicenceCheckWindow.Exists)
                //    return false;

                Playback.Wait(1000);
            }

            if (UWPApp.UwpAppRoleSelection.VisionAnywhereWindow.ItemPane.UserIDText.WaitForControlExist(3000))
            {
                // Check for the Role Selection page
                setAppointmentBook = RoleSelection(testUser, testPractice);
            }
            //else if (WpfApp.WpfAppChangedPassword.MainWindow.ChangedPasswordView.ItemPane.PreviousPasswordEdit.State != ControlStates.Offscreen)
            //{
            //    // Check for the Changed Password Page
            //    AssertAreEqualEx("Your password has changed.", WpfApp.WpfAppChangedPassword.MainWindow.ChangedPasswordView.ItemPane.YourPasswordHasChangedText.DisplayText);
            //    AssertAreEqualEx("Enter your old password to unlock the patient data stored on this machine.",
            //        WpfApp.WpfAppChangedPassword.MainWindow.ChangedPasswordView.ItemPane.EnterYourOldPasswordText.DisplayText);

            //    // View the Forgotten Old Password page
            //    Mouse.Click(WpfApp.WpfAppChangedPassword.MainWindow.ChangedPasswordView.ItemPane.ForgottenYourOldPasswordButton);
            //    AssertAreEqualEx("If you can't remember your password you can still sign in if you agree to delete all patient data from the device first.",
            //        WpfApp.WpfAppForgottenOldPassword.MainWindow.ForgottenOldPasswordView.ItemPane.ForgottenOldPasswordText.DisplayText);
            //    AssertAreEqualEx("delete patient data", WpfApp.WpfAppForgottenOldPassword.MainWindow.ForgottenOldPasswordView.ItemPane.DeletePatientDataButton.DisplayText);
            //    Mouse.Click(WpfApp.WpfAppForgottenOldPassword.MainWindow.ForgottenOldPasswordView.ItemPane.BackButton);
            //}
            else
            {
                AssertFailEx("Expected to be taken to the Role Selection or Changed Password screen");
            }

            return setAppointmentBook;
        }
        
        public bool RoleSelection(TestUser testUser, TestPractice testPractice = null)
        {
            bool setAppointmentBook = false;

            VerifyScreenElements(ElementsCheckOnRoleSelectionScreen(), "Role Selection Screen");

            // Check the correct Username is displayed
            // To showcase in report a Negative Scenario
            if (testContextInstance.TestName.Contains("NegativeScenario"))
            {
                AssertThat(VerifyCondition("nagetive_" + testUser.UserName, UWPApp.UwpAppRoleSelection.VisionAnywhereWindow.ItemPane.UserIDText.DisplayText, "Equals"), "TTOR@TEST3.com", UWPApp.UwpAppRoleSelection.VisionAnywhereWindow.ItemPane.UserIDText.DisplayText);
            }
            else
            {
                AssertThat(VerifyCondition(testUser.UserName, UWPApp.UwpAppRoleSelection.VisionAnywhereWindow.ItemPane.UserIDText.DisplayText, "Equals"), testUser.UserName, UWPApp.UwpAppRoleSelection.VisionAnywhereWindow.ItemPane.UserIDText.DisplayText);
            }

            // Set the 'Service' if required
            if (UWPApp.UwpAppRoleSelection.VisionAnywhereWindow.ItemPane.UserRolesComboBox.Enabled == true)
            {
                if (testPractice != null)
                {
                    SetExtendedComboBox(UWPApp.UwpAppRoleSelection.VisionAnywhereWindow.ItemPane.UserRolesComboBox, testPractice.ShortPracticeName, false);
                }
                else
                {
                    AssertFailEx("The Service Selection field was not set and no 'testPractice.ShortPracticeName' value was provided in the test");
                }
            }

            // Set the Appointment Book
            // TODO: Bug 10865 - Appointment Book combo box is displayed briefly when no appointment books are available
            // Test may fail here because of this
            UWPApp.UwpAppRoleSelection.VisionAnywhereWindow.ItemPane.SignInButton.WaitForControlExist(this.WaitForReadyTimeout);

            if (UWPApp.UwpAppRoleSelection.VisionAnywhereWindow.AppointmentBookComboBox.Exists)
            {
                setAppointmentBook = true;
                Mouse.Click(UWPApp.UwpAppRoleSelection.VisionAnywhereWindow.AppointmentBookComboBox);

                DateTime endTime = DateTime.Now.AddMilliseconds(Convert.ToDouble(this.WaitForReadyTimeout));

                while (DateTime.Now < endTime && UWPApp.UwpAppRoleSelection.VisionAnywhereWindow.AppointmentBookComboBox.GetChildren()[0].GetChildren().Last().Name == "Looking for appointment books")
                {
                    Playback.Wait(1000);
                }

                Keyboard.SendKeys("{ESC}");
                Playback.Wait(500);
                SetExtendedComboBox(UWPApp.UwpAppRoleSelection.VisionAnywhereWindow.AppointmentBookComboBox, testUser.DisplayName, "Appointment Book", "Selecting <b>" + testUser.DisplayName + "</b> from the Appointment book combo box", false);
            }
            else
            {
                //Nitish: NoAppointmentBooksAvailableText is not visible as of now so commenting the below statement
                //AssertIsTrueEx(UWPApp.UwpAppRoleSelection.VisionAnywhereWindow.NoAppointmentBooksAvailableText.Exists);
                //AssertAreEqualEx("There are no appointment books for this service", UWPApp.UwpAppRoleSelection.VisionAnywhereWindow.NoAppointmentBooksAvailableText.DisplayText);
            }

            // Click 'sign in'
            ClickButton(UWPApp.UwpAppRoleSelection.VisionAnywhereWindow.ItemPane.SignInButton, "SignIn Button", "SignIn button has been clicked");

            return setAppointmentBook;
        }

        public void SearchForPatient(string searchTerm)
        {
            VerifyScreenElements(ElementsCheckOnClinicianHub(), "Clinician Hub Screen");
            UWPApp.UwpAppCommon.MainWindow.GlobalSearchBox.PatientSearchTextBox.WaitForControlExist(this.WaitForReadyTimeout);
            EnterTextInEditField(UWPApp.UwpAppCommon.MainWindow.GlobalSearchBox.PatientSearchTextBox, searchTerm, "Searching Patient", "Patient's name <b>" + searchTerm + "</b> has been entered into the Patient Search box");
            Keyboard.SendKeys("{ENTER}");
            UWPApp.UwpAppSearchResults.MainWindow.PatientDataGridTable.WaitForControlExist(this.SearchTimeout);
        }

        public void SelectPatient(TestPatient testPatient)
        {
            // Search Results: Check at least 1 result is returned
            Assert.IsTrue(UWPApp.UwpAppSearchResults.MainWindow.PatientDataGridTable.PatientNameColumnHeader.WaitForControlExist(this.SearchTimeout));
            // Search Results: Select the patient record
            FindPatientOnSearchResults(testPatient);
        }

        public void FindPatientOnSearchResults(TestPatient testPatient, TestPractice testPractice = null)
        {
            // Iterate through the search results looking for a specific Identifier
            foreach (XamlControl item in UWPApp.UwpAppSearchResults.MainWindow.PatientDataGridTable.GetChildren())
            {
                if (item.ControlType.Name != "ColumnHeader")
                {
                    XamlText patientInfo = new XamlText(item);
                    XamlText identifierText = (XamlText)patientInfo.FindMatchingControls()[3];
                    if (identifierText.Name.Contains(testPatient.Identifier))
                    {
                        AssertThat(VerifyCondition(identifierText.Name, testPatient.Identifier, "Contains"), identifierText.Name, testPatient.Identifier);

                        XamlText nameText = (XamlText)patientInfo.FindMatchingControls()[0];
                        XamlText DobText = (XamlText)patientInfo.FindMatchingControls()[1];
                        XamlText genderText = (XamlText)patientInfo.FindMatchingControls()[2];

                        // Correct patient located found by identifier - now check Name and Gender/DOB
                        string expectedName = string.Format("{0}, {1}", testPatient.FamilyName.ToUpper(), testPatient.GivenName);
                        string expectedNameWithSpace = string.Format("{0} , {1}", testPatient.FamilyName.ToUpper(), testPatient.GivenName);
                        string expectedGender = string.Format("{0}", testPatient.Gender);
                        string expectedDob = string.Format("{0:dd-MMM-yyyy}", DateTime.Parse(testPatient.DateOfBirth));

                        AssertIsTrueEx(nameText.Name.StartsWith(expectedName) || nameText.Name.StartsWith(expectedNameWithSpace), string.Format("Find patient on search results: expected name '{0}', actual '{1}'", expectedName, nameText.Name));
                        AssertIsTrueEx(genderText.Name.Contains(expectedGender), string.Format("Find patient on search results: failed to find Gender '{0}' in string '{1}'", expectedGender, genderText.Name));
                        AssertIsTrueEx(DobText.Name.Contains(expectedDob), string.Format("Find patient on search results: failed to find DoB '{0}' in string '{1}'", expectedDob, DobText.Name));

                        if (testPractice != null && testPractice.IsFederatedPractice)
                        {
                            // If user is on a federated server - check Practice and Status
                            XamlText practiceText = (XamlText)patientInfo.FindMatchingControls()[4];
                            XamlText statusText = (XamlText)patientInfo.FindMatchingControls()[5];

                            AssertAreEqualEx(testPatient.ShortPracticeName, practiceText.Name);
                            AssertAreEqualEx(testPatient.RegistrationStatus ?? "Permanent", statusText.Name);
                        }
                        else
                        {
                            AssertIsTrueEx(patientInfo.FindMatchingControls().Count == 6, "Unexpected amount of controls found on patient card");
                        }

                        ClickButtonByCoords(item, "Patient Record", "Patient record with ID <b>" + testPatient.Identifier + "</b> has been selected");
                        break;
                    }
                }
            }
        }

        public void MouseClickByCoords(UITestControl control)
        {
            Microsoft.VisualStudio.TestTools.UITest.Input.Rectangle rectangle = new Microsoft.VisualStudio.TestTools.UITest.Input.Rectangle();
            rectangle = control.BoundingRectangle;
            Microsoft.VisualStudio.TestTools.UITest.Input.Point rectanglePoint = new Microsoft.VisualStudio.TestTools.UITest.Input.Point(rectangle.X + (rectangle.Width / 2), rectangle.Y + (rectangle.Height / 2));
            Mouse.Click(MouseButtons.Left, ModifierKeys.None, rectanglePoint);
        }

        public void CheckPatientDetailsOnPatientBanner(TestPatient testPatient)
        {
            VerifyScreenElements(ElementsCheckOnPatientHub(), "Patient Hub Screen");
            UWPApp.UwpAppCommon.MainWindow.PatientBanner.PatientName.WaitForControlExist(this.WaitForReadyTimeout);
            UWPApp.UwpAppCommon.MainWindow.WaitForControlExist(this.WaitForReadyTimeout);

            string expectedPatientName = string.Format("{0}, {1} ({2})", testPatient.FamilyName.ToUpper(), testPatient.GivenName, testPatient.Title);
            string expectedPatientDob = string.Format("{0:dd-MMM-yyyy} ({1}y)", DateTime.Parse(testPatient.DateOfBirth), GetPatientAge(testPatient));
            string expectedPatientGender = string.Format("{0}", testPatient.Gender);
            string expectedPatientIdentifier = string.Format("{0} {1}", testPatient.IdentifierType.ToUpper().Contains("NHS") ? "NHS" : "CHI", testPatient.Identifier);

            AssertAreEqualEx(expectedPatientName, UWPApp.UwpAppCommon.MainWindow.PatientBanner.PatientName.DisplayText);
            AssertAreEqualEx(expectedPatientDob, UWPApp.UwpAppCommon.MainWindow.PatientBanner.DisplayDateOfBirth.DisplayText);
            AssertAreEqualEx(expectedPatientGender, UWPApp.UwpAppCommon.MainWindow.PatientBanner.DisplayPersonGender.DisplayText);
            AssertAreEqualEx(expectedPatientIdentifier, UWPApp.UwpAppCommon.MainWindow.PatientBanner.DisplayPatientNumber.DisplayText);

            LogStepToReport(VerifyCondition(expectedPatientName, UWPApp.UwpAppCommon.MainWindow.PatientBanner.PatientName.DisplayText, "Equals")
                && VerifyCondition(expectedPatientDob, UWPApp.UwpAppCommon.MainWindow.PatientBanner.DisplayDateOfBirth.DisplayText, "Equals")
                && VerifyCondition(expectedPatientGender, UWPApp.UwpAppCommon.MainWindow.PatientBanner.DisplayPersonGender.DisplayText, "Equals")
                && VerifyCondition(expectedPatientIdentifier, UWPApp.UwpAppCommon.MainWindow.PatientBanner.DisplayPatientNumber.DisplayText, "Equals")
                , "Patient Details", "Patient details are corrent on the Patient Baanner", "Patient details are not corrent on the Patient Baanner");
        }

        public void SetEncounterType(string encounterType)
        {
            VerifyScreenElements(ElementsCheckOnCurrentEncounter(), "Current Encounter Screen");
            UWPApp.UwpAppCurrentEncounter.MainWindow.EncounterTypeComboBox.WaitForControlExist(this.WaitForReadyTimeout);
            System.Threading.Thread.Sleep(2000);
            UWPApp.UwpAppCurrentEncounter.MainWindow.SearchForTerm.WaitForControlExist(this.WaitForReadyTimeout);
            UWPApp.UwpAppCurrentEncounter.MainWindow.SearchForTerm.WaitForControlReady(this.WaitForReadyTimeout);
            UWPApp.UwpAppCurrentEncounter.MainWindow.SearchForTerm.WaitForControlEnabled(this.WaitForReadyTimeout);
            Mouse.Click(UWPApp.UwpAppCurrentEncounter.MainWindow.EncounterTypeComboBox);

            XamlListItem encounterTypeListItem = new XamlListItem(UWPApp.UwpAppCurrentEncounter.MainWindow.EncounterTypeComboBox.ListItem);
            encounterTypeListItem.SearchProperties[XamlListItem.PropertyNames.Name] = encounterType;
            ClickButtonByCoords(encounterTypeListItem, "EncounterType", "<b>" + encounterType + "</b> has been selected as the Encounter type");
        }

        public void SearchForQuickEntryForm(string searchTerm, string selectResultText = null)
        {
            UWPApp.UwpAppCurrentEncounter.MainWindow.SearchForTerm.WaitForControlExist();
            UWPApp.UwpAppCurrentEncounter.MainWindow.SearchForTerm.WaitForControlReady();
            UWPApp.UwpAppCurrentEncounter.MainWindow.SearchForTerm.Text = searchTerm;
            UWPApp.UwpAppCurrentEncounter.MainWindow.ItemPane.EntriesListView.WaitForControlExist(10000);

            string formType = UWPApp.UwpAppCurrentEncounter.MainWindow.ItemPane.QuickEntryFormsText.QuickEntryItemButton.Container.Name;
            if (formType == "Quick Entry Forms")
            {
                XamlText textInstance1 = new XamlText(UWPApp.UwpAppCurrentEncounter.MainWindow.ItemPane.QuickEntryFormsText.QuickEntryItemButton);
                if (textInstance1.DisplayText == searchTerm)
                    ClickButton(UWPApp.UwpAppCurrentEncounter.MainWindow.ItemPane.QuickEntryFormsText.QuickEntryItemButton, "Quick Entry Form", "Quick Entry form for <b>" + searchTerm + "</b> has been selected");
                else
                {
                    XamlText textInstance2 = new XamlText(UWPApp.UwpAppCurrentEncounter.MainWindow.ItemPane.QuickEntryFormsText.QuickEntryItemButton2);
                    if (textInstance2.DisplayText == searchTerm)
                    {
                        ClickButton(UWPApp.UwpAppCurrentEncounter.MainWindow.ItemPane.QuickEntryFormsText.QuickEntryItemButton2, "Quick Entry Form", "Quick Entry form for <b>" + searchTerm + "</b> has been selected");
                    }
                }
            }
            else
            {
                LogStepToReport(LogStatus.Fail, "Quick Entry Form selection", string.Format("Failed to find Quick Search Form: '{0}' on the Current Encounter search results list", selectResultText ?? searchTerm));
                AssertFailEx(string.Format("Failed to find Quick Search Form: '{0}' on the Current Encounter search results list", selectResultText ?? searchTerm));
            }
        }

        public void SaveEncounterDetails(TestEncounter testEncounter)
        {
            UWPApp.UwpAppCurrentEncounter.MainWindow.SaveButton.WaitForControlReady();
            ClickButton(UWPApp.UwpAppCurrentEncounter.MainWindow.SaveButton, "Save Button", "Save button has been clicked to save the encounter details");
            PostWritebackCleanup(testEncounter);
        }

        public void CheckLastEncounter(TestUser testUser, TestEncounter testEncounter, DateTime dateTimeEncounterAdded)
        {
            // Wait for the last encounter to load
            UWPApp.UwpAppPatientHub.MainWindow.PatientHubSummary.EncountersHeaderButton.WaitForControlExist(this.WaitForReadyTimeout);
            UWPApp.UwpAppPatientHub.MainWindow.PatientHubSummary.AddNewEncounterButton.WaitForControlExist(this.WaitForReadyTimeout);

            // Check the last encounter for Encounter type and Clinician Name
            AssertAreEqualEx(testEncounter.EncounterType, UWPApp.UwpAppPatientHub.MainWindow.PatientHubSummary.LastEncounterType.DisplayText);
            AssertAreEqualEx(testUser.DisplayNameV3, UWPApp.UwpAppPatientHub.MainWindow.PatientHubSummary.LastEncounterClinician.DisplayText);

            // Check the last encounter date and time
            string formattedDate = String.IsNullOrEmpty(testEncounter.StartDate) ?
                String.Format("{0:dd-MMM-yyyy}", dateTimeEncounterAdded) :
                String.Format("{0:dd-MMM-yyyy}", DateTime.Parse(testEncounter.StartDate));

            string formattedTime = String.IsNullOrEmpty(testEncounter.StartTime) ?
                String.Format("{0:HH:mm}", dateTimeEncounterAdded) :
                String.Format("{0:HH:mm}", DateTime.Parse(testEncounter.StartTime));

            AssertAreEqualEx(formattedDate.Trim(), UWPApp.UwpAppPatientHub.MainWindow.PatientHubSummary.LastEncounterDateText.DisplayText.Trim());
            AssertAreEqualEx(formattedTime.Trim(), UWPApp.UwpAppPatientHub.MainWindow.PatientHubSummary.LastEncounterTimeText.DisplayText.Trim());

            LogStepToReport(VerifyCondition(formattedDate.Trim(), UWPApp.UwpAppPatientHub.MainWindow.PatientHubSummary.LastEncounterDateText.DisplayText.Trim(), "Equals")
                && VerifyCondition(formattedTime.Trim(), UWPApp.UwpAppPatientHub.MainWindow.PatientHubSummary.LastEncounterTimeText.DisplayText.Trim(), "Equals")
                && VerifyCondition(testEncounter.EncounterType, UWPApp.UwpAppPatientHub.MainWindow.PatientHubSummary.LastEncounterType.DisplayText, "Equals")
                && VerifyCondition(testUser.DisplayNameV3, UWPApp.UwpAppPatientHub.MainWindow.PatientHubSummary.LastEncounterClinician.DisplayText, "Equals")
                , "Last Encounter", "Last Encounter details are correct", "Last Encounter details are not correct");
        }

        #endregion

        #region Peak Flow

        public void EnterPeakFlowEntryDetails(TestPeakFlow testExamination)
        {
            VerifyScreenElements(ElementsCheckOnPeakFlowScreen(), "Peak Flow Screen");
            UWPApp.UwpAppAddPeakFlow.MainWindow.PeakFlowPageTitle.WaitForControlExist(this.WaitForReadyTimeout);
            UWPApp.UwpAppAddPeakFlow.MainWindow.DescriptionComboBox.WaitForControlExist(this.WaitForReadyTimeout);

            // Select Clinical term (Description) if required
            if (testExamination.ClinicalTerm != UWPApp.UwpAppAddPeakFlow.MainWindow.DescriptionComboBox.SelectedItem)
            {
                SelectItemFromCombobox(UWPApp.UwpAppAddPeakFlow.MainWindow.DescriptionComboBox, testExamination.ClinicalTerm, "Description Combo Box", "<b>" + testExamination.ClinicalTerm + "</b> has been selected for Description Combobox");
            }    

            // Set Flow rate value
            EnterTextInEditField(UWPApp.UwpAppAddPeakFlow.MainWindow.ItemPane.FlowRateTextBoxEdit, testExamination.Result.ToString(), "Flow Rate", "<b>" + testExamination.Result.ToString() + "</b> has been entered in the FlowRate text box");

            // Enter Notes if required
            if (!String.IsNullOrEmpty(testExamination.Notes))
            {
                EnterTextInEditField(UWPApp.UwpAppAddPeakFlow.MainWindow.ItemPane.NotesFieldEdit, testExamination.Notes.ToString(), "Notes", "<b>" + testExamination.Notes.ToString() + "</b> has been entered in the Notes text box");
            }
        }

        public void SavePeakFlowEntryDetails()
        {
            UWPApp.UwpAppAddPeakFlow.MainWindow.SaveButton.EnsureClickable();
            ClickButton(UWPApp.UwpAppAddPeakFlow.MainWindow.SaveButton, "Save Button", "Save button for Peak Flow entry has been clicked");
        }

        public void CheckPeakFlowEntryOnCurrentEncounter(TestEncounter testEncounter, TestPeakFlow testExamination)
        {
            UWPApp.UwpAppCurrentEncounter.MainWindow.EntriesListView.WaitForControlExist();

            // Find correct entry in the list of entries
            bool entryFound = false;

            foreach (XamlListItem entry in UWPApp.UwpAppCurrentEncounter.MainWindow.EntriesListView.Items)
            {
                XamlText clinicalTerm = new XamlText(entry);
                clinicalTerm.SearchProperties[XamlText.PropertyNames.Instance] = "1";

                if (clinicalTerm.DisplayText == testExamination.ClinicalTerm)
                {
                    entryFound = true;

                    // Validate the values of the other fields
                    XamlText result = new XamlText(entry);
                    result.SearchProperties[XamlText.PropertyNames.Instance] = "2";
                    XamlText category = new XamlText(entry);
                    category.SearchProperties[XamlText.PropertyNames.Instance] = "4";

                    AssertAreEqualEx(testExamination.ClinicalTerm, clinicalTerm.DisplayText);
                    AssertAreEqualEx(string.Format("{0} L/min", testExamination.Result), result.DisplayText);

                    LogStepToReport(VerifyCondition(clinicalTerm.DisplayText, testExamination.ClinicalTerm, "Equals")
                        && VerifyCondition(testExamination.ClinicalTerm, clinicalTerm.DisplayText, "Equals")
                        && VerifyCondition(string.Format("{0} L/min", testExamination.Result), result.DisplayText, "Equals")
                        , "Peak Flow Entry on Current Encounter", "Peak Flow entry details are correctly reflected on Current Encounter screen"
                        , "Peak Flow entry details are not correctly reflected on Current Encounter screen");

                    if (!string.IsNullOrEmpty(testExamination.Notes))
                    {
                        XamlText notes = new XamlText(entry);
                        notes.SearchProperties[XamlText.PropertyNames.Instance] = "3";
                        AssertAreEqualEx(testExamination.Notes, notes.DisplayText);
                    }

                    AssertAreEqualEx("Examination", category.DisplayText);
                    break;
                }
            }

            if (!entryFound)
            {
                LogStepToReport(LogStatus.Fail, "Peak Flow Entry on Current Encounter", "Peak Flow entry not found on Current Encounter Entries List");
                AssertFailEx("Peak Flow entry not found on Current Encounter Entries List");
            }
        }

        public void CheckPeakFlowEntryOnLastEncounter(TestPeakFlow testExamination)
        {
            // Find correct entry in the list of entries
            bool entryFound = false;

            foreach (XamlText entryDetails in UWPApp.UwpAppPatientHub.MainWindow.PatientHubSummary.LastEncounterEntry.FindMatchingControls())
            {
                if (entryDetails.DisplayText.Contains(testExamination.ClinicalTerm))
                {
                    entryFound = true;
                    AssertAreEqualEx(string.Format("{0} {1} L/min", testExamination.ClinicalTerm, testExamination.Result), entryDetails.DisplayText);
                    LogStepToReport(VerifyCondition(string.Format("{0} {1} L/min", testExamination.ClinicalTerm, testExamination.Result), entryDetails.DisplayText, "Equals")
                        , "Peak Flow entry on Last Encounter", "Peak flow entry details are correctly reflected on Patient Hub Last Encounter"
                        , "Peak flow entry details are not correctly reflected on Patient Hub Last Encounter");

                    break;
                }
            }

            if (!entryFound)
            {
                LogStepToReport(LogStatus.Fail, "Peak Flow entry on Last Encounter", string.Format("Peak Flow entry for {0} not found on Patient Hub Last Encounter", testExamination.ClinicalTerm));
                AssertFailEx(string.Format("Peak Flow entry for {0} not found on Patient Hub Last Encounter", testExamination.ClinicalTerm));
            }
        }

        public void CheckPeakFlowEntryOnPatientHub(TestEncounter testEncounter, TestPeakFlow testExamination, TestUser testUser, DateTime dateTimeEncounterAdded)
        {
            // Find correct tile in the list of tiles
            UWPApp.UwpAppPatientHub.MainWindow.PatientHubSummary.LifestyleAndExaminationHeaderButton.WaitForControlReady(this.WaitForReadyTimeout);
            UWPApp.UwpAppPatientHub.MainWindow.PatientHubSummary.LifestyleAndExaminationHeaderButton.EnsureClickable();

            Boolean tileFound = false;
            Boolean listFound = false;
            UITestControlCollection collection = UWPApp.UwpAppPatientHub.MainWindow.ItemList.FindMatchingControls();

            foreach (XamlList currentList in collection)
            {
                foreach (XamlListItem currentItem in currentList.Items)
                {
                    Object tileTitle = currentItem.GetType().GetProperty("FirstChild", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(currentItem, null);
                    if (((UITestControl)tileTitle).Name.Equals("Peak flow"))
                    {
                        listFound = true;
                        tileFound = true;
                        // Get the value and date text
                        Object tileDescription = tileTitle.GetType().GetProperty("NextSibling", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(tileTitle, null);
                        AssertAreEqualEx(string.Format("{0} L/min", testExamination.Result), ((UITestControl)tileDescription).Name);

                        Object tileDate = tileDescription.GetType().GetProperty("NextSibling", BindingFlags.Instance | BindingFlags.NonPublic).GetValue(tileDescription, null);
                        string formattedDate = String.IsNullOrEmpty(testExamination.EntryDate) ?
                            String.Format("{0:dd-MMM-yyyy}", dateTimeEncounterAdded) :
                            String.Format("{0:dd-MMM-yyyy}", DateTime.Parse(testExamination.EntryDate));

                        AssertAreEqualEx(formattedDate, ((UITestControl)tileDate).Name);

                        LogStepToReport(VerifyCondition(((UITestControl)tileTitle).Name, "Peak flow", "Equals")
                            && VerifyCondition(string.Format("{0} L/min", testExamination.Result), ((UITestControl)tileDescription).Name, "Equals")
                            && VerifyCondition(formattedDate, ((UITestControl)tileDate).Name, "Equals")
                            , "Peak flow tile", "Peak flow entry is displayed correctly on Patient Hub Examinations and Lifestyles section"
                            , "Peak flow entry is not displayed correctly on Patient Hub Examinations and Lifestyles section");

                        break;
                    }
                }
                if (listFound)
                {
                    break;
                }
            }

            if (!tileFound)
            {
                LogStepToReport(LogStatus.Fail, "Peak flow tile", "Peak flow Tile not found on Patient Hub Examinations and Lifestyles section");
                AssertFailEx("Peak flow Tile not found on Patient Hub Examinations and Lifestyles section");
            }
        }

        #endregion
    }
}