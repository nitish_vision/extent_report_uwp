﻿namespace PatientHub.Framework.UWP.UITest
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Diagnostics;
    using System.IO;

    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Microsoft.VisualStudio.TestTools.UITest.Input;
    using Microsoft.VisualStudio.TestTools.UITesting.WindowsRuntimeControls;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;

    using PatientHub.Framework.FunctionalTesting.Test.DataAccess;
    using RelevantCodes.ExtentReports;

    public partial class UwpAppBase
    {
        #region Public methods

        /// <summary>
        /// Close all open application windows
        /// </summary>
        public void CloseAllInstances(string processName)
        {
            Process[] allMatchingProcesses = Process.GetProcessesByName(processName);

            foreach (Process proc in allMatchingProcesses)
            {
                proc.Kill();
            }

            Playback.Wait(1000);
        }

        /// <summary>
        /// Compares expected list of values with actual collection of XamlText controls
        /// </summary>
        public void CompareStringLists(List<string> expectedList, UITestControlCollection actualList, string listName)
        {
            for (int i = 0; i < expectedList.Count; i++)
            {
                this.AssertAreEqualEx(expectedList[i], TryGetListItem(actualList, i), "List = " + listName);
            }

            if (actualList.Count > expectedList.Count)
            {
                for (int i = expectedList.Count; i < actualList.Count; i++)
                {
                    this.AssertFailEx(string.Format("Unexpected list item: \'{0}\', Line {1}, List {2}", actualList[i], i + 1, listName));
                }
            }

            return;
        }

        public TestMedication CreateAcuteVersionOfRepeat(TestMedication testMedication)
        {
            TestMedication acuteMedication = new TestMedication()
            {
                MedicationType = "Acute Medication",
                DrugName = testMedication.DrugName,
                DosageInstructions = testMedication.DosageInstructions,
                DosageInstructionsDefault = testMedication.DosageInstructionsDefault,
                DrugClass = testMedication.DrugClass,
                DrugClassDefault = testMedication.DrugClassDefault,
                Code = testMedication.Code,
                Quantity = testMedication.Quantity,
                QuantityDefault = testMedication.QuantityDefault,
                Preparation = testMedication.Preparation,
                PreparationDefault = testMedication.PreparationDefault,
                InPractice = testMedication.InPractice
            };

            return acuteMedication;
        }

        /// <summary>
        /// Delete the local Patient Hub Framework database
        /// </summary>
        public void DeleteLocalDatabase()
        {
            // Delete all matching patient hub dbs for now
            // TODO - We could find the correct database for the current user and only delete this one

            string dataDirectoryFullPath = Environment.ExpandEnvironmentVariables(
                Path.Combine(ConfigurationManager.AppSettings["AppLocalPackagePath"]));

            if (Directory.Exists(dataDirectoryFullPath))
            {
                IEnumerable<string> matchingFiles = Directory.EnumerateFiles(dataDirectoryFullPath,
                    ConfigurationManager.AppSettings["localPatientHubDbFilename"]);

                foreach (string fileName in matchingFiles)
                {
                    if (File.Exists(fileName))
                    {
                        File.Delete(fileName);
                        Debug.WriteLine("DeleteLocalDatabase successful - End");
                    }
                    else
                    {
                        Debug.WriteLine("DeleteLocalDatabase not required - Database not found");
                    }
                }
            }
        }

        public int GetPatientAge(TestPatient testPatient)
        {
            // Calculating the age which includes checking whether their birthday has passed
            DateTime dateOfBirth = DateTime.Parse(testPatient.DateOfBirth);
            int age = DateTime.Now.Year - dateOfBirth.Year;

            if (dateOfBirth.AddYears(age) >= DateTime.Now)
            {
                age--;
            }

            return age;
        }

        /// <summary>
        /// Select menu item from the Top App Bar Navigation
        /// </summary>
        //public void NavBarSelectMenuItem(string menuItemName)
        //{
        //    // Expand the App Nav Bar
        //    if (!StoreApp.StoreAppCommon.VisionAnywhereWindow.NavBar.NavBarTopRow.Exists)
        //    {
        //        Keyboard.SendKeys("Z", ModifierKeys.Windows);
        //    }

        //    if (menuItemName == "Home" || menuItemName == "Appointments" || menuItemName == "Patient Summary")
        //    {
        //        foreach (XamlListItem navBarButton in StoreApp.StoreAppCommon.VisionAnywhereWindow.NavBar.NavBarTopRow.GetChildren())
        //        {
        //            UITestControl navButton = navBarButton.GetChildren()[0];
        //            if (navButton.GetChildren()[1].Name == menuItemName)
        //            {
        //                navButton.EnsureClickable();
        //                Gesture.Tap(navButton);
        //                return;
        //            }
        //        }
        //    }
        //    else
        //    {
        //        StoreApp.StoreAppCommon.VisionAnywhereWindow.NavBar.NavBarTopRow.PatientSummaryMenuItem.SubItemsToggleButton.WaitForControlExist(5000);

        //        if (!StoreApp.StoreAppCommon.VisionAnywhereWindow.NavBar.NavBarTopRow.PatientSummaryMenuItem.SubItemsToggleButton.Pressed)
        //        {
        //            Gesture.Tap(StoreApp.StoreAppCommon.VisionAnywhereWindow.NavBar.NavBarTopRow.PatientSummaryMenuItem.SubItemsToggleButton);
        //        }

        //        StoreApp.StoreAppCommon.VisionAnywhereWindow.NavBar.NavBarBottomRow.WaitForControlExist(5000);
        //        Playback.Wait(1000);

        //        foreach (XamlListItem navBarButton in StoreApp.StoreAppCommon.VisionAnywhereWindow.NavBar.NavBarBottomRow.GetChildren())
        //        {
        //            UITestControl navButton = navBarButton.GetChildren()[0];
        //            if (navButton.Name == menuItemName)
        //            {
        //                navButton.EnsureClickable();
        //                Gesture.Tap(navButton);
        //                return;
        //            }
        //        }
        //    }

        //    throw new Exception(string.Format("Unable to find Top App Bar menu item {0}", menuItemName));
        //}

        /// <summary>
        /// Carry out data cleanup after saving an Encounter to account for mismatches between what we send to the web service
        /// and what is then returned by the service in the Patient Record
        /// </summary>
        /// <param name="testEncounter"></param>
        public void PostWritebackCleanup(TestEncounter testEncounter)
        {
            // Remove the 'note' suffix from the end of the Encounter Type string as the data returned from the web service does not contain this string
            string noteSuffix = " note";

            if (testEncounter.EncounterType.EndsWith(noteSuffix))
            {
                testEncounter.EncounterType = testEncounter.EncounterType.Remove(testEncounter.EncounterType.LastIndexOf(noteSuffix));
            }
        }

        public void RestartVisionAnywhere()
        {
            // Close the app
            this.CloseAllInstances("PatientHub.Framework.Uwp");

            // Launch the app
            this.AppWindow = XamlWindow.Launch(ConfigurationManager.AppSettings["AppTileAutomationId"]);
            this.AppWindow.CloseOnPlaybackCleanup = true;
        }

        /// <summary>
        /// Select menu item
        /// </summary>
        public void SelectMenuItem(XamlMenu menu, string menuItemName)
        {
            XamlMenuItem menuItem = new XamlMenuItem(menu);
            menuItem.SearchProperties[XamlControl.PropertyNames.Name] = menuItemName;
            Gesture.Tap(menuItem);
        }

        /// <summary>
        /// Allows us to select a value from an 'ExtendedComboBox' type control
        /// </summary>
        /// <param name="comboBox">The ComboBox control</param>
        /// <param name="listItemString">The list item to select from the ComboBox</param>
        /// <param name="exactMatch">Match the string exactly, or look for a case insensitive 'contains' match</param>
        public void SetExtendedComboBox(XamlComboBox comboBox, string listItemString, bool exactMatch = true)
        {
            UITestControlCollection listItems = comboBox.Items;
            int i = 0;

            Gesture.Tap(comboBox, new Point(10, 10));

            foreach (XamlListItem listItem in listItems)
            {
                try
                {
                    if ((exactMatch && listItem.GetChildren()[0].Name == listItemString) || (!exactMatch && listItem.GetChildren()[0].Name.ToLower().Contains(listItemString.ToLower())))
                    {
                        Keyboard.SendKeys("{ESC}");
                        Gesture.Tap(comboBox, new Point(10, 10));
                        Gesture.Tap(listItem);

                        return;
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine(string.Format("Failed at index: {0}.\nError: {1}", i, e.Message));
                }

                i++;
            }
            
            Assert.Fail(string.Format("Could not find the list item '{0}' in ComboBox '{1}'", listItemString, comboBox.AutomationId ?? comboBox.GetProperty("QueryId")));
        }

        public void SetExtendedComboBox(XamlComboBox comboBox, string listItemString, string stepname, string details, bool exactMatch = true)
        {
            UITestControlCollection listItems = comboBox.Items;
            int i = 0;

            Gesture.Tap(comboBox, new Point(10, 10));

            foreach (XamlListItem listItem in listItems)
            {
                try
                {
                    if ((exactMatch && listItem.GetChildren()[0].Name == listItemString) || (!exactMatch && listItem.GetChildren()[0].Name.ToLower().Contains(listItemString.ToLower())))
                    {
                        Keyboard.SendKeys("{ESC}");
                        Gesture.Tap(comboBox, new Point(10, 10));
                        Gesture.Tap(listItem);
                        LogStepToReport(LogStatus.Info, stepname, details);
                        return;
                    }
                }
                catch (Exception e)
                {
                    Debug.WriteLine(string.Format("Failed at index: {0}.\nError: {1}", i, e.Message));
                }

                i++;
            }

            Assert.Fail(string.Format("Could not find the list item '{0}' in ComboBox '{1}'", listItemString, comboBox.AutomationId ?? comboBox.GetProperty("QueryId")));
        }


        /// <summary>
        /// Swipe list item to the Right to select it
        /// </summary>
        /// <param name="parentList"></param>
        /// <param name="listItemName"></param>
        public void SwipeRightListItem(XamlList parentList, string listItemName)
        {
            XamlListItem listItem = new XamlListItem(parentList);
            listItem.SearchProperties[XamlText.PropertyNames.Name] = listItemName;
            Gesture.Swipe(listItem, UITestGestureDirection.Right);
        }

        /// <summary>
        /// Tap the 'Add new encounter' button on Patient Summary page
        /// </summary>
        //public void TapAddNewEncounterButton()
        //{
        //    ////Tapping button directly didn't work - the control appears to be bigger than the object that is visible on screen
        //    ////so suspect this has something to do with it.  Tapping the centre point of the BoundingRectangle seems to work though.
        //    //Rectangle rectangle = new Rectangle();
        //    //rectangle = StoreApp.StoreAppPatientHub.VisionAnywhereWindow.PatientSummaryHub.EncountersHubSection.AddEncounterButton.BoundingRectangle;

        //    //Point rectanglePoint = new Point(rectangle.X + (rectangle.Width / 2), rectangle.Y + (rectangle.Height / 2));
        //    //Gesture.Tap(rectanglePoint);
        //    ////Mouse.Click(rectanglePoint);

        //    // Click in Search field
        //    // Tab 5 times to get to Add new encounter button
        //    // Press Enter key to select Add new encounter button

        //    // Add a delay here to see if we can get around the app crash problem when clicking 'Add new encounter' button
        //    Playback.Wait(2000);

        //    Gesture.Tap(StoreApp.StoreAppCommon.VisionAnywhereWindow.SearchBoxGroup.SearchBoxEdit);

        //    for (int i = 0; i < 10; i++)
        //    {
        //        Keyboard.SendKeys("{TAB}");

        //        if (StoreApp.StoreAppPatientHub.VisionAnywhereWindow.PatientSummaryHub.EncountersHubSection.AddEncounterButton.HasFocus)
        //        {
        //            Keyboard.SendKeys("{ENTER}");
        //            break;
        //        }
        //    }
        //}

        /// <summary>
        /// Tap on a control using the coordinates (use if Coded UI sees blocking window in front of control)
        /// </summary>
        /// <param name="control"></param>
        public void TapControlByCoords(UITestControl control)
        {
            Rectangle rectangle = control.BoundingRectangle;
            Point rectanglePoint = new Point(rectangle.X + (rectangle.Width / 2), rectangle.Y + (rectangle.Height / 2));
            Gesture.Tap(rectanglePoint);
        }

        /// <summary>
        /// Tap list item
        /// </summary>
        /// <param name="parentList"></param>
        /// <param name="listItemName"></param>
        public void TapListItem(XamlList parentList, string listItemName)
        {
            XamlListItem listItem = new XamlListItem(parentList);
            listItem.SearchProperties[XamlText.PropertyNames.Name] = listItemName;
            listItem.EnsureClickable();
            Gesture.Tap(listItem);
        }

        /// <summary>
        /// Returns the text of a control at a given index in a XamlText collection, but check whether it exists first
        /// </summary>
        private string TryGetListItem(UITestControlCollection list, int index)
        {
            // Check whether the control collection contains an item at the given index number
            if (index >= list.Count)
            {
                return string.Format("Line {0} Not Found", index);
            }
            else
            {
                if (list[index].SearchProperties.Contains("DisplayText"))
                {
                    return ((XamlText)list[index]).DisplayText;
                }
                else
                {
                    return list[index].Name;
                }
            }
        }

        #endregion  

        #region Custom Assert methods

        public void AssertAreEqualEx(bool expected, bool actual, string failureInfo = "")
        {
            try
            {
                Assert.AreEqual(expected, actual);

            }
            catch (AssertFailedException afe)
            {
                if (this.ContinueOnAssertFailure)
                {
                    afe.Data.Add("AssertFailLocation", this.GetDiagnosticsInformationForCurrentFrame());
                    afe.Data.Add("FailureInfo", failureInfo);
                    this.Failures.Add(afe);
                }
                else
                {
                    throw afe;
                }
            }
        }

        public void AssertAreEqualEx(string expected, string actual, string failureInfo = "")
        {
            try
            {
                Assert.AreEqual(expected, actual);
            }
            catch (AssertFailedException afe)
            {
                if (this.ContinueOnAssertFailure)
                {
                    afe.Data.Add("AssertFailLocation", this.GetDiagnosticsInformationForCurrentFrame());
                    afe.Data.Add("FailureInfo", failureInfo);
                    this.Failures.Add(afe);
                }
                else
                {
                    throw afe;
                }
            }
        }

        public void AssertAreEqualEx(int expected, int actual, string failureInfo = "")
        {
            try
            {
                Assert.AreEqual(expected, actual);

            }
            catch (AssertFailedException afe)
            {
                if (this.ContinueOnAssertFailure)
                {
                    afe.Data.Add("AssertFailLocation", this.GetDiagnosticsInformationForCurrentFrame());
                    afe.Data.Add("FailureInfo", failureInfo);
                    this.Failures.Add(afe);
                }
                else
                {
                    throw afe;
                }
            }
        }

        public void AssertAreEqualEx(double expected, System.Nullable<double> actual, string failureInfo = "")
        {
            try
            {
                Assert.AreEqual(expected, actual);

            }
            catch (AssertFailedException afe)
            {
                if (this.ContinueOnAssertFailure)
                {
                    afe.Data.Add("AssertFailLocation", this.GetDiagnosticsInformationForCurrentFrame());
                    afe.Data.Add("FailureInfo", failureInfo);
                    this.Failures.Add(afe);
                }
                else
                {
                    throw afe;
                }
            }
        }

        public void AssertAreEqualEx(DateTime expected, DateTime actual, string failureInfo = "")
        {
            try
            {
                Assert.AreEqual(expected, actual);

            }
            catch (AssertFailedException afe)
            {
                if (this.ContinueOnAssertFailure)
                {
                    afe.Data.Add("AssertFailLocation", this.GetDiagnosticsInformationForCurrentFrame());
                    afe.Data.Add("FailureInfo", failureInfo);
                    this.Failures.Add(afe);
                }
                else
                {
                    throw afe;
                }
            }
        }

        public void AssertAreEqualEx(TimeSpan expected, TimeSpan actual, string failureInfo = "")
        {
            try
            {
                Assert.AreEqual(expected, actual);

            }
            catch (AssertFailedException afe)
            {
                if (this.ContinueOnAssertFailure)
                {
                    afe.Data.Add("AssertFailLocation", this.GetDiagnosticsInformationForCurrentFrame());
                    afe.Data.Add("FailureInfo", failureInfo);
                    this.Failures.Add(afe);
                }
                else
                {
                    throw afe;
                }
            }
        }

        public void AssertFailEx(string failureInfo)
        {
            try
            {
                throw new AssertFailedException(failureInfo);
            }
            catch (AssertFailedException afe)
            {
                if (this.ContinueOnAssertFailure)
                {
                    afe.Data.Add("AssertFailLocation", this.GetDiagnosticsInformationForCurrentFrame());
                    afe.Data.Add("FailureInfo", failureInfo);
                    this.Failures.Add(afe);
                }
                else
                {
                    throw afe;
                }
            }
        }

        public void AssertIsNullEx(object objectToTest, string failureInfo = "")
        {
            try
            {
                if (objectToTest != null)
                {
                    throw new AssertFailedException(string.Format("{0} - Expected:null, Actual:{1}", objectToTest.GetType().Name, objectToTest.ToString()));
                }
            }
            catch (AssertFailedException afe)
            {
                if (this.ContinueOnAssertFailure)
                {
                    afe.Data.Add("AssertFailLocation", this.GetDiagnosticsInformationForCurrentFrame());
                    afe.Data.Add("FailureInfo", failureInfo);
                    this.Failures.Add(afe);
                }
                else
                {
                    throw afe;
                }
            }
        }

        public void AssertIsTrueEx(bool expression, string failureInfo = "")
        {
            try
            {
                Assert.IsTrue(expression);

            }
            catch (AssertFailedException afe)
            {
                if (this.ContinueOnAssertFailure)
                {
                    afe.Data.Add("AssertFailLocation", this.GetDiagnosticsInformationForCurrentFrame());
                    afe.Data.Add("FailureInfo", failureInfo);
                    this.Failures.Add(afe);
                }
                else
                {
                    throw afe;
                }
            }
        }

        public string GetDiagnosticsInformationForCurrentFrame()
        {
            var callSite = (new StackTrace(1, true)).GetFrame(1);

            string fileName = callSite.GetFileName();
            int lineNumber = callSite.GetFileLineNumber();
            string methodName = callSite.GetMethod().Name;
            return string.Format(
                "Assert Failed at {0}:l.{1}, Method:{2}",
                fileName,
                lineNumber,
                methodName
            );
        }

        public void ThrowAllAssertFails(List<AssertFailedException> failures)
        {
            if (failures != null)
            {
                if (failures.Count > 0)
                {
                    string allFailuresMessage = String.Empty;

                    foreach (AssertFailedException failure in failures)
                    {
                        allFailuresMessage += "\r\nASSERTFAILEDEXCEPTION\r\n";
                        allFailuresMessage += failure.Message;
                        allFailuresMessage += "\r\n";
                        allFailuresMessage += failure.Data["AssertFailLocation"].ToString();
                        allFailuresMessage += "\r\n";
                        allFailuresMessage += failure.Data["FailureInfo"].ToString();
                        allFailuresMessage += "\r\n";
                    }

                    Assert.Fail(allFailuresMessage);
                }
            }
        }

        #endregion

        #region Custom Action methods

        public void EnterTextInEditField(XamlEdit edit, string value, string stepName, string details)
        {
            edit.DrawHighlight();
            edit.Text = value;
            LogStepToReport(LogStatus.Info,stepName,details);
        }

        public void ClickButton(XamlButton button, string stepName, string details)
        {
            //button.DrawHighlight();
            Mouse.Click(button);
            LogStepToReport(LogStatus.Info, stepName, details);
        }

        public bool VerifyCondition(string value1, string value2, string action)
        {
            if (action == "Contains")
            {
                if (value1.Contains(value2))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                if (value1 == value2)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public void ClickButtonByCoords(UITestControl control, string stepName, string details)
        {
            Microsoft.VisualStudio.TestTools.UITest.Input.Rectangle rectangle = new Microsoft.VisualStudio.TestTools.UITest.Input.Rectangle();
            rectangle = control.BoundingRectangle;
            Microsoft.VisualStudio.TestTools.UITest.Input.Point rectanglePoint = new Microsoft.VisualStudio.TestTools.UITest.Input.Point(rectangle.X + (rectangle.Width / 2), rectangle.Y + (rectangle.Height / 2));
            Mouse.Click(MouseButtons.Left, ModifierKeys.None, rectanglePoint);
            LogStepToReport(LogStatus.Info, stepName, details);
        }

        public void SelectItemFromCombobox(XamlComboBox comboBox, string value, string stepName, string details)
        {
            comboBox.SelectedItem = value;
            LogStepToReport(LogStatus.Info, stepName, details);
        }

        public void VerifyScreenElements(bool condition, string screen)
        {
            AssertThat(condition, screen + " loaded successfully", screen + " is not loaded successfully");
        }

        #endregion
    }
}
