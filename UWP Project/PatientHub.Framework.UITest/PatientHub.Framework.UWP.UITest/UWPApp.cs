﻿namespace PatientHub.Framework.UWP.UITest
{
    using System;
    using PatientHub.Framework.UWP.UITest.UIMaps.UwpAppCommonClasses;
    using PatientHub.Framework.UWP.UITest.UIMaps.UwpAppLoginClasses;
    using PatientHub.Framework.UWP.UITest.UIMaps.UwpAppRoleSelectionClasses;
    using PatientHub.Framework.UWP.UITest.UIMaps.UwpAppSearchResultsClasses;
    using PatientHub.Framework.UWP.UITest.UIMaps.UwpAppPatientHubClasses;
    using PatientHub.Framework.UWP.UITest.UIMaps.UwpAppCurrentEncounterClasses;
    using PatientHub.Framework.UWP.UITest.UIMaps.UwpAppAddPeakFlowClasses;

    public class UWPApp
    {

        #region Fields

        /// <summary>
        /// UWP App Login UIMap
        /// </summary>
        private static UwpAppLogin uwpAppLogin;

        /// <summary>
        /// Uwp App Role Selection UIMap
        /// </summary>
        private static UwpAppRoleSelection uwpAppRoleSelection;

        /// <summary>
        /// Uwp App Common UIMap
        /// </summary>
        private static UwpAppCommon uwpAppCommon;

        /// <summary>
        /// Uwp App Search Results UIMap
        /// </summary>
        private static UwpAppSearchResults uwpAppSearchResults;

        /// <summary>
        /// Uwp App Patient Hub UIMap
        /// </summary>
        private static UwpAppPatientHub uwpAppPatientHub;

        /// <summary>
        /// Uwp App Current Encounter UIMap
        /// </summary>
        private static UwpAppCurrentEncounter uwpAppCurrentEncounter;

        /// <summary>
        /// Uwp App Peak Flow UIMap
        /// </summary>
        private static UwpAppAddPeakFlow uwpAppAddPeakFlow;
        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets UWP App Login UIMap
        /// </summary>
        public static UwpAppLogin UwpAppLogin
        {
            get
            {
                if (uwpAppLogin == null)
                {
                    return new UwpAppLogin();
                }
                return uwpAppLogin;
            }

            set
            {
                UWPApp.uwpAppLogin = value;
            }
        }

        /// <summary>
        /// Gets or sets Uwp App Role Selection UIMap
        /// </summary>
        public static UwpAppRoleSelection UwpAppRoleSelection
        {
            get
            {
                if (uwpAppRoleSelection == null)
                {
                    return new UwpAppRoleSelection();
                }

                return uwpAppRoleSelection;
            }

            set
            {
                UWPApp.uwpAppRoleSelection = value;
            }
        }

        /// <summary>
        /// Gets or sets Uwp App Common UIMap
        /// </summary>
        public static UwpAppCommon UwpAppCommon
        {
            get
            {
                if (uwpAppCommon == null)
                {
                    return new UwpAppCommon();
                }

                return uwpAppCommon;
            }

            set
            {
                UWPApp.uwpAppCommon = value;
            }
        }

        /// <summary>
        /// Gets or sets Uwp App Search Results UIMap
        /// </summary>
        public static UwpAppSearchResults UwpAppSearchResults
        {
            get
            {
                if (uwpAppSearchResults == null)
                {
                    return new UwpAppSearchResults();
                }

                return uwpAppSearchResults;
            }

            set
            {
                UWPApp.uwpAppSearchResults = value;
            }
        }

        /// <summary>
        /// Gets or sets Uwp App Patient Hub UIMap
        /// </summary>
        public static UwpAppPatientHub UwpAppPatientHub
        {
            get
            {
                if (uwpAppSearchResults == null)
                {
                    return new UwpAppPatientHub();
                }

                return uwpAppPatientHub;
            }

            set
            {
                UWPApp.uwpAppPatientHub = value;
            }
        }

        /// <summary>
        /// Gets or sets Uwp App Current Encounter UIMap
        /// </summary>
        public static UwpAppCurrentEncounter UwpAppCurrentEncounter
        {
            get
            {
                if (uwpAppSearchResults == null)
                {
                    return new UwpAppCurrentEncounter();
                }

                return uwpAppCurrentEncounter;
            }

            set
            {
                UWPApp.uwpAppCurrentEncounter = value;
            }
        }

        /// <summary>
        /// Gets or sets Uwp App Add Peak Flow UIMap
        /// </summary>
        public static UwpAppAddPeakFlow UwpAppAddPeakFlow
        {
            get
            {
                if (uwpAppSearchResults == null)
                {
                    return new UwpAppAddPeakFlow();
                }

                return uwpAppAddPeakFlow;
            }

            set
            {
                UWPApp.uwpAppAddPeakFlow = value;
            }
        }

        #endregion

    }
}
