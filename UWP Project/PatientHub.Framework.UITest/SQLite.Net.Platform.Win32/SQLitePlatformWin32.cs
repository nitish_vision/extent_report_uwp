﻿//------------------------------------------------------------------
//
// Copyright (c) 2012 - 2014 Adaptive Apps Ltd. All rights reserved.
//
// MvvmCross is licensed using Microsoft Public License (Ms-PL)
// Copyright (c) Cirrious Ltd. http://www.cirrious.com
//
//------------------------------------------------------------------

namespace SQLite.Net.Platform.Win32
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Security.Cryptography;
    using System.Threading.Tasks;

    using SQLite.Net.Interop;

    /// <summary>
    /// The SQLite platform win 32.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class SQLitePlatformWin32 : ISQLitePlatform
    {
        /// <summary>
        /// The delete retry attempts
        /// </summary>
        private readonly int deleteRetryAttempts = 10;

        /// <summary>
        /// The delete retry interval
        /// </summary>
        private readonly int deleteRetryInterval = 700;

        /// <summary>
        /// Initializes a new instance of the <see cref="SQLitePlatformWin32" /> class.
        /// </summary>
        public SQLitePlatformWin32()
        {
            this.SQLiteApi = new SQLiteApiWin32();
            this.StopwatchFactory = new StopwatchFactoryWin32();
            this.ReflectionService = new ReflectionServiceWin32();
            this.VolatileService = new VolatileServiceWin32();
        }

        /// <summary>
        /// Gets the name of the application.
        /// </summary>
        /// <value>String representing the application name.</value>
        public static string AppName
        {
            get
            {
                return AppDomain.CurrentDomain.FriendlyName.Replace(".vshost.exe", string.Empty).Replace(".exe", string.Empty);
            }
        }

        /// <summary>
        /// Gets the data directory.
        /// </summary>
        /// <value>String representing the data directory.</value>
        public static string DataDirectory
        {
            get
            {
                var baseDirectory = AppDomain.CurrentDomain.BaseDirectory;
                return Path.Combine(baseDirectory, "data");
            }
        }

        /// <summary>
        /// Gets the reflection service.
        /// </summary>
        /// <value>The reflection service.</value>
        public IReflectionService ReflectionService { get; private set; }

        /// <summary>
        /// Gets the SQLite API.
        /// </summary>
        /// <value>The SQLite API.</value>
        public ISQLiteApi SQLiteApi { get; private set; }

        /// <summary>
        /// Gets the stopwatch factory.
        /// </summary>
        /// <value>The stopwatch factory.</value>
        public IStopwatchFactory StopwatchFactory { get; private set; }

        /// <summary>
        /// Gets the volatile service.
        /// </summary>
        /// <value>The volatile service.</value>
        public IVolatileService VolatileService { get; private set; }

        /// <summary>
        /// The copy lookup databases to storage.
        /// </summary>
        /// <param name="filenames">The filenames.</param>
        /// <returns>The <see cref="Task" />.</returns>
        public async Task<bool> CopyLookupDatabasesToStorage(string[] filenames)
        {
            try
            {
                foreach (var filename in filenames)
                {
                    // If the file is a database file and does not exist in the install location, copy it
                    if (filename.EndsWith(".db"))
                    {
                        var destinationFilePath = this.GetLocalFilePath(filename);
                        var sourceFilePath = Path.Combine(DataDirectory, filename);

                        var fileExists = await this.FileExists(destinationFilePath);
                        if (!fileExists)
                        {
                            File.Copy(sourceFilePath, destinationFilePath);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("An exception occurred when trying to copy the read code file {0}", ex.Message);
                throw;
            }

            return true;
        }

        /// <summary>
        /// Deletes the database file.
        /// </summary>
        /// <param name="fullPathFilename">The full path filename.</param>
        /// <returns>The <see cref="Task" /> of bool for success or failure.</returns>
        public async Task<bool> DeleteDatabaseFile(string fullPathFilename)
        {
            bool retVal = false;
            int attempts = 0;

            while(!retVal
                && attempts <= this.deleteRetryAttempts)
            {
                try
                {
                    File.Delete(fullPathFilename);
                    retVal = true;
                    break;
                }
                catch (Exception)
                {
                    retVal = false;
                }

                attempts++;
                await Task.Delay(this.deleteRetryInterval);
            }

            return retVal;
        }

        /// <summary>
        /// Gets the latest file version.
        /// </summary>
        /// <param name="fileStartsWith">The file starts with.</param>
        /// <returns>The latest file version, or null if it does not exist.</returns>
        /// <remarks>Assumes the file format is NAME_VERSION.db where version is an integer</remarks>
        public async Task<int> GetLatestFileVersion(string fileStartsWith)
        {
            var versionNumber = 0;
            var fileList = Directory.GetFiles(this.GetLocalFileDirectory());

            foreach (var file in fileList)
            {
                var fileInfo = new FileInfo(file);

                if (fileInfo.Name.StartsWith(fileStartsWith))
                {
                    int fileVersion;
                    var fileVersionLength = fileInfo.Name.Length - fileStartsWith.Length - fileInfo.Extension.Length - 1;
                    var fileVersionPart = fileInfo.Name.Substring(fileStartsWith.Length + 1, fileVersionLength);

                    if (int.TryParse(fileVersionPart, out fileVersion) && fileVersion > versionNumber)
                    {
                        versionNumber = fileVersion;
                    }
                }
            }

            return versionNumber;
        }

        /// <summary>
        /// The delete old version of file if exists.
        /// </summary>
        /// <param name="oldFileStartsWith">The old file starts with.</param>
        /// <param name="latestFilename">The latest filename.</param>
        /// <returns>The <see cref="Task" />.</returns>
        public async Task<bool> DeleteOldVersionOfFileIfExists(string oldFileStartsWith, string latestFilename)
        {
            var fileList = Directory.GetFiles(this.GetLocalFileDirectory());

            foreach (var file in fileList)
            {
                var fileInfo = new FileInfo(file);
                if (fileInfo.Name.StartsWith(oldFileStartsWith) && fileInfo.Name != latestFilename)
                {
                    fileInfo.Delete();
                    return true;
                }
            }

            return false;
        }

        public async Task<bool> DeleteExpiredDatabases(DateTime oldestDatabaseUpdatedDateTime, string patientDbNameRoot)
        {
            var fileList = Directory.GetFiles(this.GetLocalFileDirectory());

            foreach (var file in fileList)
            {
                var fileInfo = new FileInfo(file);

                if (fileInfo.Name.StartsWith(patientDbNameRoot))
                {
                    
                    if (oldestDatabaseUpdatedDateTime.CompareTo(fileInfo.LastWriteTime) > 0)
                    {
                        try
                        {
                            File.Delete(file);
                            
                        }
                        catch (Exception)
                        {
                            Debug.WriteLine("Could not delete - maybe another process deleted, renamed or moved the file");
                        }
                    }
                }
            }

            return true;
        }

        public async Task<bool> DeleteObsoleteDatabases(string patientDbNameRoot, List<string> supportedUpgradePaths)
        {
            var fileList = Directory.GetFiles(this.GetLocalFileDirectory());

            foreach (var file in fileList)
            {
                var fileInfo = new FileInfo(file);

                if (fileInfo.Name.StartsWith(patientDbNameRoot))
                {
                    var deleteThis = true;
                    foreach (var version in supportedUpgradePaths)
                    {
                        if (file.EndsWith(version + ".db"))
                        {
                            deleteThis = false;
                        }
                    }

                    if (deleteThis)
                    {
                        try
                        {
                            File.Delete(file);
                        }
                        catch (Exception)
                        {
                            Debug.WriteLine("Could not delete - maybe another process deleted, renamed or moved the file");
                        }

                    }
                }
            }

            return true;
        }

        public async Task<bool> ReversionDatabase(string oldDbName, string newDbName)
        {
            try
            {
                File.Move(oldDbName, newDbName);
                return true;
            }
            catch (System.UnauthorizedAccessException)
            {
                return false;
            }
            catch (FileNotFoundException)
            {
                return false;
            }
        }

        /// <summary>
        /// Checks whether a file exists on the relevant platform
        /// </summary>
        /// <param name="fullPathFilename">The full path to the file</param>
        /// <returns>True if exists</returns>
        public async Task<bool> FileExists(string fullPathFilename)
        {
            return await Task.Run(() => File.Exists(fullPathFilename));
        }

        /// <summary>
        /// Returns a platform-specific local file path.
        /// </summary>
        /// <param name="filename">The filename.</param>
        /// <returns>The <see cref="string" />.</returns>
        public string GetLocalFilePath(string filename)
        {
            return Path.Combine(this.GetLocalFileDirectory(), filename);
        }

        /// <summary>
        /// The get random byte array.
        /// </summary>
        /// <param name="length">The length.</param>
        /// <returns>The byte array.</returns>
        public byte[] GetRandomByteArray(uint length)
        {
            var crypto = new RNGCryptoServiceProvider();
            byte[] result = new byte[length];
            crypto.GetBytes(result);
            return result;             
        }

        /// <summary>
        /// The get random hex string.
        /// </summary>
        /// <param name="length">The length.</param>
        /// <returns>The <see cref="string" />.</returns>
        public string GetRandomHexString(uint length)
        {
            var s = BitConverter.ToString(this.GetRandomByteArray(length));
            return s.Replace("-", "");
        }
        
        private string GetLocalFileDirectory()
        {
            var path = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                "VisionAnywhere",
                "data");

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            return path;
        }
    }
}

// ReSharper restore InconsistentNaming