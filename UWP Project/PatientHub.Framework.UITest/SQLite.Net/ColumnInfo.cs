﻿namespace SQLite.Net
{
    using Attributes;

    public class ColumnInfo
    {
        //			public int cid { get; set; }

        [Column("name")]
        public string Name { get; set; }

        //			[Column ("type")]
        //			public string ColumnType { get; set; }

        public int notnull { get; set; }

        //			public string dflt_value { get; set; }

        //			public int pk { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
