﻿//
// Copyright (c) 2013 Øystein Krog (oystein.krog@gmail.com)
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

using System.Threading.Tasks;

namespace SQLite.Net.Interop
{
    using System;
    using System.Collections.Generic;

    public interface ISQLitePlatform
    {
        ISQLiteApi SQLiteApi { get; }
        IStopwatchFactory StopwatchFactory { get; }
        IReflectionService ReflectionService { get; }
        IVolatileService VolatileService { get; }

        string GetRandomHexString(uint length);

        byte[] GetRandomByteArray(uint length);

        string GetLocalFilePath(string filename);

        Task<bool> FileExists(string fullPathFilename);

        Task<bool> DeleteDatabaseFile(string fullPathFilename);

        Task<bool> CopyLookupDatabasesToStorage(string [] filenames);

        Task<int> GetLatestFileVersion(string fileStartsWith);

        Task<bool> DeleteOldVersionOfFileIfExists(string oldFileStartsWith, string latestFilename);

        Task<bool> DeleteExpiredDatabases(DateTime oldestDatabaseUpdatedDateTime, string patientDbNameRoot);

        Task<bool> DeleteObsoleteDatabases(string patientDbNameRoot, List<string> supportedUpgradePaths);

        Task<bool> ReversionDatabase(string oldDbName, string newDbName);
    }
}