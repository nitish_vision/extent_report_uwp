﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using SQLite.Net.Attributes;

    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class TestRequest : TestEntryBase
    {
        private string actionDate;

        /// <summary>
        /// Gets or sets the Urgency.
        /// </summary>
        public string Urgency { get; set; }

        /// <summary>
        /// Gets or sets the Action Date.
        /// </summary>
        public string ActionDate {
            get
            {
                return this.actionDate;
            }

            set
            {
                if (value != null)
                {
                    if (value.StartsWith("@Today"))
                    {
                        if (value == "@Today")
                        {
                            this.actionDate = DateTime.Today.ToShortDateString();
                        }
                        else
                        {
                            this.actionDate = DateTime.Today.AddDays(Int32.Parse(value.Substring(6))).ToShortDateString();
                        }
                    }
                    else
                    {
                        this.actionDate = value;
                    }
                }
                else
                {
                    this.actionDate = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the Request System.
        /// </summary>
        public string TestRequestSystem { get; set; }

    }
}
