﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using SQLite.Net.Attributes;

    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class TestSmokingDrinking : TestEntryBase
    {
        /// <summary>
        /// Gets or sets the ClinicalCode.
        /// </summary>
        public string ClinicalCode { get; set; }

        /// <summary>
        /// Gets or sets the Clinical Term.
        /// </summary>
        public string ClinicalTerm { get; set; }

        /// <summary>
        /// Gets or sets the Status (lifestyle only).
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the Value1.
        /// </summary>
        public double Value1 { get; set; }

        /// <summary>
        /// Gets or sets the Value2.
        /// </summary>
        public double Value2 { get; set; }

        /// <summary>
        /// Gets or sets the Value3.
        /// </summary>
        public double Value3 { get; set; }

        /// <summary>
        /// Gets or sets the Start Date.
        /// </summary>
        public string StartDate { get; set; }

        /// <summary>
        /// Gets or sets the Stop Date.
        /// </summary>
        public string StopDate { get; set; }
    }
}
