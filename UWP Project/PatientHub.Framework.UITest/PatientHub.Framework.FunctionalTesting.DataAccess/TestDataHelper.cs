﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using SQLite.Net;
    using SQLite.Net.Interop;

    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public static class TestDataHelper
    {
        private static Random randomPatientId = new Random();

        public static List<TestMedicalHistory> GetAllMedicalHistoryForPatient(ISQLitePlatform sqlitePlatform, int patientId)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestMedicalHistory> retVal = connection.Query<TestMedicalHistory>(
                @"SELECT pc.ClinicalCode, pc.ClinicalTerm from PdsCondition pc where pc.PatientID = ?;", patientId);

            foreach (TestMedicalHistory entry in retVal)
            {
                Debug.WriteLine(string.Format("Conditions retrieved for PatientId {0}: {1}, {2}",
                    patientId, entry.ClinicalCode, entry.ClinicalTerm));
            }

            connection.Close();
            connection.Dispose();

            return retVal;
        }

        public static List<TestAllergy> GetAllAllergiesForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestAllergy> retVal = connection.Query<TestAllergy>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                a.ClinicalTermSearchTerm, a.ClinicalTerm, a.ClinicalCode, a.DrugNameSearchTerm, a.DrugName, a.DrugCode, a.ReactionType, a.Certainty, a.Severity, a.ReactionClinicalTerm, a.ReactionClinicalCode, 
                a.AllergyDescription, a.Notes   
                from EncounterEntry ee 
                inner join Allergy a on ee.AllergyID = a.ID
                where ee.EncounterID = ?;", encounterId);

            foreach (TestAllergy entry in retVal)
            {
                if (!string.IsNullOrEmpty(entry.ClinicianName))
                {
                    entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
                }

                Debug.WriteLine(
                    string.Format(
                        "Allergy retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}",
                        encounterId, entry.QuickListSelection, entry.ClinicalTermSearchTerm, entry.ClinicalTerm, entry.ClinicalCode,
                        entry.DrugNameSearchTerm, entry.DrugName, entry.DrugCode,
                        entry.ReactionType, entry.Certainty, entry.Severity, entry.ReactionClinicalTerm,
                        entry.ReactionClinicalCode, entry.Notes, entry.ClinicianName, entry.EntryDate, entry.EntryTime));
            }

            connection.Close();
            connection.Dispose();

            return retVal;
        }

        public static List<TestAnkleNeuropathy> GetAllAnkleNeuropathyForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestAnkleNeuropathy> retVal = connection.Query<TestAnkleNeuropathy>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                an.ClinicalCode, an.ClinicalTerm, an.RightJerk, an.LeftJerk, an.RightVib, an.LeftVib, an.Notes
                from EncounterEntry ee 
                inner join AnkleNeuropathy an on ee.AnkleNeuropathyID = an.ID
                where ee.EncounterID = ?;", encounterId);

            foreach (TestAnkleNeuropathy entry in retVal)
            {
                if (!string.IsNullOrEmpty(entry.ClinicianName))
                {
                    entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
                }

                Debug.WriteLine(string.Format("AnkleNeuropathy retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}",
                    encounterId, entry.QuickListSelection, entry.ClinicalCode, entry.ClinicalTerm, entry.RightJerk, entry.LeftJerk, entry.RightVib, 
                    entry.LeftVib, entry.Notes, entry.ClinicianName, entry.EntryDate, entry.EntryTime, entry.Private, entry.InPractice));
            }

            connection.Close();
            connection.Dispose();

            return retVal;
        }

        public static List<TestBloodPressure> GetAllBloodPressureForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestBloodPressure> retVal = connection.Query<TestBloodPressure>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                bp.ClinicalTerm, bp.ClinicalCode, bp.Systolic, bp.Diastolic, bp.Korotkoff, bp.Laterality, bp.Posture, bp.Cuff, bp.BodyText, bp.Notes
                from EncounterEntry ee 
                inner join BloodPressure bp on ee.BloodPressureId = bp.ID
                where ee.EncounterID = ? order by ee.Sequence;", encounterId);

            foreach (TestBloodPressure entry in retVal)
            {
                if (!string.IsNullOrEmpty(entry.ClinicianName))
                {
                    entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
                }

                Debug.WriteLine(string.Format("Blood Pressure retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}",
                    encounterId, entry.QuickListSelection, entry.ClinicalTerm, entry.ClinicalCode, entry.Systolic,
                    entry.Diastolic, entry.Korotkoff, entry.Laterality, entry.Posture, entry.Cuff, entry.BodyText, entry.Notes, entry.ClinicianName, entry.EntryDate, entry.EntryTime,
                    entry.InPractice, entry.Private));
            }

            connection.Close();
            connection.Dispose();

            return retVal;
        }

        public static List<TestMedicalHistory> GetAllConditionsForPatient(ISQLitePlatform sqlitePlatform, string patientId)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestMedicalHistory> retVal = connection.Query<TestMedicalHistory>(
                @"SELECT pc.ClinicalCode, pc.ClinicalTerm from PdsCondition pc where pc.IsIncludedInRequest = 1 and pc.PatientID = ?;", patientId);

            foreach (TestMedicalHistory entry in retVal)
            {
                Debug.WriteLine(string.Format("Conditions retrieved for PatientId {0}: {1}, {2}",
                    patientId, entry.ClinicalCode, entry.ClinicalTerm));
            }

            connection.Close();
            connection.Dispose();

            return retVal;
        }
        
        public static List<TestPatientContactInfo> GetAllContactInfoForPatient(ISQLitePlatform sqlitePlatform, string patientId)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestPatientContactInfo> retVal = connection.Query<TestPatientContactInfo>(
                @"SELECT ID, ContactType, ContactValue, IsAddress
                from PatientContactInfo where PatientID = ?;", patientId);

            connection.Close();
            connection.Dispose();

            foreach (TestPatientContactInfo contactInfo in retVal)
            {
                Debug.WriteLine(string.Format("Contact info retrieved for Patient Id {0}: {1}, {2}, {3}",
                    patientId, contactInfo.ContactType, contactInfo.ContactValue, contactInfo.IsAddress));
            }

            return retVal;
        }

        public static List<TestContraception> GetAllContraceptionForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestContraception> retVal = connection.Query<TestContraception>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                c.ClinicalCode, c.ClinicalTerm, c.ServiceType, c.IUCDate, c.Notes
                from EncounterEntry ee 
                inner join Contraception c on ee.ContraceptionID = c.ID
                where ee.EncounterID = ?;", encounterId);

            foreach (TestContraception entry in retVal)
            {
                if (!string.IsNullOrEmpty(entry.ClinicianName))
                {
                    entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
                }

                Debug.WriteLine(string.Format("Contraception retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}",
                    encounterId, entry.QuickListSelection, entry.ClinicalCode, entry.ClinicalTerm, entry.ServiceType, entry.IUCDate, entry.Notes,
                    entry.ClinicianName, entry.EntryDate, entry.EntryTime, entry.Private, entry.InPractice));
            }

            connection.Close();
            connection.Dispose();

            return retVal;
        }

        public static List<DataEntrySearch> GetAllSearchesForTestCase(ISQLitePlatform sqlitePlatform, int testCaseId)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<DataEntrySearch> retVal = connection.Query<DataEntrySearch>(
                @"SELECT des.SearchTerm, des.ExpectedResultCount, des.ExpectedResults from DataEntrySearch des where des.TestCaseID = ?;", testCaseId);

            foreach (DataEntrySearch entry in retVal)
            {
                Debug.WriteLine(string.Format("Search data retrieved for TestCaseId {0}: {1}, {2}, {3}",
                    testCaseId, entry.SearchTerm, entry.ExpectedResultCount, entry.ExpectedResults));
            }

            connection.Close();
            connection.Dispose();

            return retVal;
        }

        public static List<TestEncounter> GetAllEncountersForPatient(ISQLitePlatform sqlitePlatform, int patientId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestEncounter> retVal = connection.Query<TestEncounter>(
                "SELECT ID, EncounterType, EncounterCode, StartDate, StartTime, EndTime, ClinicianName, Sequence from Encounter where PatientID = ?;", patientId);

            foreach (TestEncounter encounter in retVal)
            {
                if (!string.IsNullOrEmpty(encounter.ClinicianName))
                {
                    encounter.ClinicianName = encounter.ClinicianName.Replace("@RegisteredGP", registeredGp);
                }
            }

            connection.Close();
            connection.Dispose();
            return retVal;
        }

        public static List<TestExamination> GetAllExaminationsForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestExamination> retVal = connection.Query<TestExamination>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                e.ClinicalTerm, e.ClinicalCode, e.Value1, e.Notes
                from EncounterEntry ee 
                inner join Examination e on ee.ExaminationID = e.ID
                where ee.EncounterID = ? order by ee.Sequence;", encounterId);

            foreach (TestExamination entry in retVal)
            {
                if (!string.IsNullOrEmpty(entry.ClinicianName))
                {
                    entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
                }

                Debug.WriteLine(string.Format("Examination retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}",
                    encounterId, entry.QuickListSelection, entry.ClinicalTerm, entry.ClinicalCode, 
                    entry.Value1, entry.Notes, entry.ClinicianName, entry.EntryDate, entry.EntryTime, 
                    entry.InPractice, entry.Private));
            }

            connection.Close();
            connection.Dispose();

            return retVal;
        }

        public static List<TestMedicalHistory> GetAllExcludedConditionsForPatient(ISQLitePlatform sqlitePlatform, string patientId)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestMedicalHistory> retVal = connection.Query<TestMedicalHistory>(
                @"SELECT pc.ClinicalCode, pc.ClinicalTerm from PdsCondition pc where pc.IsIncludedInRequest = 0 and pc.PatientID = ?;", patientId);

            foreach (TestMedicalHistory entry in retVal)
            {
                Debug.WriteLine(string.Format("Excluded Conditions retrieved for PatientId {0}: {1}, {2}",
                    patientId, entry.ClinicalCode, entry.ClinicalTerm));
            }

            connection.Close();
            connection.Dispose();

            return retVal;
        }

        public static List<TestFootPulse> GetAllFootPulseForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestFootPulse> retVal = connection.Query<TestFootPulse>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                fp.ClinicalCode, fp.ClinicalTerm, fp.DP, fp.PT, fp.Notes
                from EncounterEntry ee 
                inner join FootPulse fp on ee.FootPulseID = fp.ID
                where ee.EncounterID = ?;", encounterId);

            foreach (TestFootPulse entry in retVal)
            {
                if (!string.IsNullOrEmpty(entry.ClinicianName))
                {
                    entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
                }

                Debug.WriteLine(string.Format("FootPulse retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}",
                    encounterId, entry.QuickListSelection, entry.ClinicalCode, entry.ClinicalTerm, entry.DP, entry.PT, entry.Notes,
                    entry.Private, entry.InPractice, entry.ClinicianName, entry.EntryDate, entry.EntryTime));
            }

            connection.Close();
            connection.Dispose();

            return retVal;
        }

        public static List<TestGeneralEntry> GetAllGeneralEntriesForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestGeneralEntry> retVal = connection.Query<TestGeneralEntry>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice, 
                ge.SearchTerm, ge.ClinicalTerm, ge.ClinicalCode, ge.Priority, ge.EpisodeType, ge.CategoryBeforeWriteBack, 
                ge.Category, ge.Characteristic, ge.EndDate, ge.Notes, ge.Value, ge.Unit, ge.ValueFieldMinValue, ge.ValueFieldMaxValue
                from EncounterEntry ee 
                inner join GeneralEntry ge on ee.GeneralEntryID = ge.ID
                where ee.EncounterID = ?;", encounterId);

            foreach (TestGeneralEntry entry in retVal)
            {
                if (!string.IsNullOrEmpty(entry.ClinicianName))
                {
                    entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
                }

                Debug.WriteLine(string.Format("General Entry retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}, {18}, {19}",
                    encounterId, entry.QuickListSelection, entry.SearchTerm, entry.ClinicalTerm, entry.ClinicalCode, entry.Notes, entry.Priority,
                    entry.EpisodeType, entry.CategoryBeforeWriteBack, entry.Category, entry.Characteristic, entry.Private, entry.InPractice, entry.ClinicianName, entry.EntryDate, 
                    entry.EntryTime, entry.Value, entry.Unit, entry.ValueFieldMinValue, entry.ValueFieldMaxValue));
            }

            connection.Close();
            connection.Dispose();

            return retVal;
        }

        public static List<TestHealthBoard> GetAllHealthBoardsForTestCase(ISQLitePlatform sqlitePlatform, int testCaseId)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestHealthBoard> retVal = connection.Query<TestHealthBoard>(
                "SELECT HealthBoard.ID, HealthBoardName, Region, Code from TestCaseHealthBoardMapping join HealthBoard on TestCaseHealthBoardMapping.HealthBoardID = HealthBoard.ID where TestCaseID = ?;", testCaseId);

            // Default user is UserId 1 = NHS England
            if (retVal.Count == 0)
            {
                retVal = connection.Query<TestHealthBoard>("SELECT HealthBoard.ID, HealthBoardName, Region, Code from HealthBoard where HealthBoard.ID = 1");
            }

            connection.Close();
            connection.Dispose();

            foreach (TestHealthBoard healthBoard in retVal)
            {
                Debug.WriteLine(string.Format("Healthboard retrieved for TestCaseId {0}: {1}, {2}, {3}",
                    testCaseId, healthBoard.Region, healthBoard.HealthBoardName, healthBoard.Code));
            }

            return retVal;
        }

        public static List<TestHeartExamination> GetAllHeartExaminationsForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestHeartExamination> retVal = connection.Query<TestHeartExamination>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                he.ClinicalCode, he.ClinicalTerm, he.Rhythmn, he.Size, he.Sounds, he.Notes
                from EncounterEntry ee 
                inner join HeartExamination he on ee.HeartExaminationID = he.ID
                where ee.EncounterID = ?;", encounterId);

            foreach (TestHeartExamination entry in retVal)
            {
                if (!string.IsNullOrEmpty(entry.ClinicianName))
                {
                    entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
                }

                Debug.WriteLine(string.Format("HeartExamination retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}",
                    encounterId, entry.QuickListSelection, entry.ClinicalCode, entry.ClinicalTerm, entry.Rhythmn, entry.Size, entry.Sounds, entry.Notes,
                    entry.ClinicianName, entry.EntryDate, entry.EntryTime, entry.Private, entry.InPractice));
            }

            connection.Close();
            connection.Dispose();

            return retVal;
        }

        public static List<TestImmunisation> GetAllImmunisationsForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestImmunisation> retVal = connection.Query<TestImmunisation>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                i.SearchTerm, i.ImmunisationType, i.ClinicalTerm, i.ClinicalCode, i.Source, i.BatchNo, i.Method, i.Site, i.Reason, i.ReasonText, 
                i.DueDate, i.Notes, i.Stage, i.StageValue, i.Status 
                from EncounterEntry ee 
                inner join Immunisation i on ee.ImmunisationId = i.ID
                where ee.EncounterID = ?;", encounterId);

            foreach (TestImmunisation entry in retVal)
            {
                if (!string.IsNullOrEmpty(entry.ClinicianName))
                {
                    entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
                }

                Debug.WriteLine(string.Format("Immunisation retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}, {18}, {19}",
                    encounterId, entry.QuickListSelection, entry.SearchTerm, entry.ImmunisationType, entry.ClinicalTerm, entry.ClinicalCode, 
                    entry.Stage, entry.StageValue, entry.Status, entry.Source, entry.BatchNo, entry.Method, entry.Site, entry.Reason, entry.ReasonText, entry.DueDate,
                    entry.Notes, entry.ClinicianName, entry.EntryDate, entry.EntryTime));
            }

            connection.Close();
            connection.Dispose();

            return retVal;
        }

        public static List<TestLifestyle> GetAllLifestyleForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestLifestyle> retVal = connection.Query<TestLifestyle>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                l.ClinicalCode, l.ClinicalTerm, l.Type, l.Notes
                from EncounterEntry ee 
                inner join Lifestyle l on ee.LifestyleID = l.ID
                where ee.EncounterID = ?;", encounterId);

            foreach (TestLifestyle entry in retVal)
            {
                if (!string.IsNullOrEmpty(entry.ClinicianName))
                {
                    entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
                }

                Debug.WriteLine(string.Format("Lifestyle retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}",
                    encounterId, entry.QuickListSelection, entry.ClinicalCode, entry.ClinicalTerm, entry.Type, entry.Notes,
                    entry.ClinicianName, entry.EntryDate, entry.EntryTime, entry.Private, entry.InPractice));
            }

            connection.Close();
            connection.Dispose();

            return retVal;
        }

        public static List<TestMedicalHistory> GetAllMedicalHistoryForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestMedicalHistory> retVal = connection.Query<TestMedicalHistory>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                mh.ClinicalCode, mh.ClinicalTerm, mh.ClinicalCode2, mh.ClinicalTerm2, mh.Notes
                from EncounterEntry ee 
                inner join MedicalHistory mh on ee.MedicalHistoryID = mh.ID
                where ee.EncounterID = ?;", encounterId);

            foreach (TestMedicalHistory entry in retVal)
            {
                if (!string.IsNullOrEmpty(entry.ClinicianName))
                {
                    entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
                }

                Debug.WriteLine(string.Format("MedicalHistory retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}",
                    encounterId, entry.QuickListSelection, entry.ClinicalCode, entry.ClinicalTerm, entry.ClinicalCode2, entry.ClinicalTerm2, entry.Notes,
                    entry.ClinicianName, entry.EntryDate, entry.EntryTime, entry.Private, entry.InPractice));
            }

            connection.Close();
            connection.Dispose();

            return retVal;
        }

        public static List<TestMedication> GetAllMedicationsForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestMedication> retVal = connection.Query<TestMedication>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                m.MedicationType, m.DrugName, m.Code, m.Quantity, m.QuantityDefault, m.AvailablePackSizes, m.PackSize, m.DrugClassDefault, m.DrugClass, 
                m.DosageInstructionsDefault, m.DosageInstructions, m.StandardDosageDefault, m.StandardDosage, m.UserDefinedDosage, m.PreparationDefault, m.Preparation, 
                m.SearchTerm, m.BrowseFolders, m.NumberOfRepeats, m.IssueNumber, m.TotalIssues, m.PrescribedDate, m.LastIssueDate, m.RepeatUntilDate,
                m.IsGeneric, m.IsControlled, m.IsHighRisk, m.IsDiscontinued, m.EligibleForIssue, m.EligibleForReauth, 
                m.IssueWarnings, m.ReauthWarnings, m.HighRiskWarning
                from EncounterEntry ee inner join Medication m on ee.MedicationID = m.ID where ee.EncounterID = ? order by ee.Sequence;", encounterId);

            foreach (TestMedication medication in retVal)
            {
                if (!string.IsNullOrEmpty(medication.ClinicianName))
                {
                    medication.ClinicianName = medication.ClinicianName.Replace("@RegisteredGP", registeredGp);
                }

                Debug.WriteLine(string.Format("Medication retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}, {18}, {19}, {20}, {21}, {22}, {23}, {24}, {25}, {26}", 
                    encounterId, medication.SearchTerm,
                    medication.BrowseFolders, medication.MedicationType, medication.DrugName, medication.Code, medication.DrugClassDefault, medication.DrugClass,
                    medication.DosageInstructionsDefault, medication.DosageInstructions, medication.StandardDosageDefault, medication.StandardDosage, medication.UserDefinedDosage,
                    medication.QuantityDefault, medication.Quantity, medication.AvailablePackSizes, medication.PackSize,
                    medication.PreparationDefault, medication.Preparation, medication.RepeatUntilDate, medication.NumberOfRepeats,
                    medication.IssueNumber, medication.TotalIssues, medication.ClinicianName, medication.EntryDate, medication.EntryTime, medication.HighRiskWarning));
            }

            connection.Close();
            connection.Dispose();

            return retVal;
        }

        public static List<TestPassiveSmoking> GetAllPassiveSmokingForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestPassiveSmoking> retVal = connection.Query<TestPassiveSmoking>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                ps.ClinicalCode, ps.ClinicalTerm, ps.PassiveSmoking, ps.Cause, ps.Notes
                from EncounterEntry ee 
                inner join PassiveSmoking ps on ee.PassiveSmokingID = ps.ID
                where ee.EncounterID = ?;", encounterId);

            foreach (TestPassiveSmoking entry in retVal)
            {
                if (!string.IsNullOrEmpty(entry.ClinicianName))
                {
                    entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
                }

                Debug.WriteLine(string.Format("PassiveSmoking retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}",
                    encounterId, entry.QuickListSelection, entry.ClinicalCode, entry.ClinicalTerm, entry.PassiveSmoking, entry.Cause, entry.Notes,
                    entry.ClinicianName, entry.EntryDate, entry.EntryTime, entry.Private, entry.InPractice));
            }

            connection.Close();
            connection.Dispose();

            return retVal;
        }

        public static List<TestPatient> GetAllPatientsForTestCase(ISQLitePlatform sqlitePlatform, int testCaseId)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());

            List<TestPatient> patients = connection.Query<TestPatient>(
                "SELECT PatientID as ID from TestCasePatientMapping where TestCaseID = ?;", testCaseId);

            List<TestPatient> retVal = new List<TestPatient>();

            foreach (TestPatient patient in patients)
            {
                if (patient.ID == "@AnyLeeds")
                {
                    // Pick a random PatientID in the Leeds range of patients
                    Debug.WriteLine("RANDOM PATIENT TO BE SELECTED FROM LEEDS PRACTICE");
                    int id = randomPatientId.Next(7000, 10103);

                    List<TestPatient> retVal1 = connection.Query<TestPatient>(
                        "SELECT p.ID, p.FamilyName, p.GivenName, p.Title, p.Identifier, p.IdentifierType, p.DateOfBirth, p.Gender, p.RegisteredGP, p.UsualGP, p.Postcode, p.PracticeId, pr.PracticeName, pr.ShortPracticeName from "
                        + GetPatientTableName() + " p inner join " + GetPracticeTableName() + " pr on p.PracticeID = pr.ID where p.ID = ?;", id);
                    retVal.AddRange(retVal1);
                }
                else
                {
                    List<TestPatient> retVal1 = connection.Query<TestPatient>(
                        "SELECT p.ID, p.FamilyName, p.GivenName, p.Title, p.Identifier, p.IdentifierType, p.DateOfBirth, p.Gender, p.RegisteredGP, p.UsualGP, p.Postcode, p.PracticeId, pr.PracticeName, pr.ShortPracticeName from "
                        + GetPatientTableName() + " p inner join " + GetPracticeTableName() + " pr on p.PracticeID = pr.ID where p.ID = ?;", patient.ID.ToString());
                    retVal.AddRange(retVal1);
                }

                Debug.WriteLine(string.Format("Patient retrieved for test case {0}: {1}, {2} ({3}), {4}, {5}, {7}={6}, {8}, {9}, {10}, {11}, {12}", testCaseId, patient.FamilyName,
                    patient.GivenName, patient.Title, patient.DateOfBirth, patient.Gender, patient.Identifier, patient.IdentifierType, patient.RegisteredGP, patient.UsualGP, patient.Postcode, patient.PracticeName, patient.ShortPracticeName));
            }

            connection.Close();
            connection.Dispose();
            return retVal;
        }

        public static List<TestPractice> GetAllPracticesForTestCase(ISQLitePlatform sqlitePlatform, int testCaseId)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());

            List<TestPractice> retVal = connection.Query<TestPractice>(
                "SELECT p.ID, PracticeName, ShortPracticeName, HealthBoard, IsFederatedPractice from TestCasePracticeMapping join " + GetPracticeTableName() + " p on TestCasePracticeMapping.PracticeID = p.ID where TestCaseID = ?;",
                testCaseId);

            foreach (TestPractice practice in retVal)
            {
                Debug.WriteLine(string.Format("TestPractice retrieved for Test Case Id {0}: {1}, {2}, {3}, {4}", testCaseId,
                    practice.PracticeName, practice.ShortPracticeName, practice.HealthBoard, practice.IsFederatedPractice));
            }

            connection.Close();
            connection.Dispose();

            return retVal;
        }

        public static List<TestPeakFlow> GetAllPeakFlowsForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        { 
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestPeakFlow> retVal = connection.Query<TestPeakFlow>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                pf.ClinicalTerm, pf.ClinicalCode, pf.Result, pf.Unit, pf.Operator, pf.ResultQualifier, pf.WarningFlag, pf.DeviceType, pf.Notes
                from EncounterEntry ee 
                inner join PeakFlow pf on ee.PeakFlowID = pf.ID
                where ee.EncounterID = ?;", encounterId);

            foreach (TestPeakFlow entry in retVal)
            {
                if (!string.IsNullOrEmpty(entry.ClinicianName))
                {
                    entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
                }

                Debug.WriteLine(
                    string.Format("PeakFlow retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}",
                        encounterId, entry.QuickListSelection, entry.ClinicalTerm, entry.ClinicalCode, entry.Result, entry.Unit, 
                        entry.ResultQualifier, entry.WarningFlag, entry.DeviceType, entry.Notes,
                        entry.ClinicianName, entry.EntryDate, entry.EntryTime));
            }

            connection.Close();
            connection.Dispose();

            return retVal;
        }

        public static List<TestReferral> GetAllReferralsForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestReferral> retVal = connection.Query<TestReferral>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                r.ClinicalCode, r.ClinicalTerm, r.Source, r.ReferralType, r.Attendance, r.Urgency, r.ActionDate, r.Notes
                from EncounterEntry ee 
                inner join Referral r on ee.ReferralID = r.ID
                where ee.EncounterID = ?;", encounterId);

            foreach (TestReferral entry in retVal)
            {
                if (!string.IsNullOrEmpty(entry.ClinicianName))
                {
                    entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
                }

                Debug.WriteLine(string.Format("Referral retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}",
                    encounterId, entry.QuickListSelection, entry.ClinicalCode, entry.ClinicalTerm, entry.Source, entry.ReferralType, entry.Attendance, entry.Urgency, entry.ActionDate, entry.Notes,
                    entry.ClinicianName, entry.EntryDate, entry.EntryTime, entry.Private, entry.InPractice));
            }

            connection.Close();
            connection.Dispose();

            return retVal;
        }

        public static List<TestRequest> GetAllRequestsForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestRequest> retVal = connection.Query<TestRequest>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                r.ClinicalCode, r.ClinicalTerm, r.Urgency, r.ActionDate, r.Notes
                from EncounterEntry ee 
                inner join Request r on ee.RequestID = r.ID
                where ee.EncounterID = ?;", encounterId);

            foreach (TestRequest entry in retVal)
            {
                if (!string.IsNullOrEmpty(entry.ClinicianName))
                {
                    entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
                }

                Debug.WriteLine(string.Format("Request retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}",
                    encounterId, entry.QuickListSelection, entry.ClinicalCode, entry.ClinicalTerm, entry.Urgency, entry.ActionDate, entry.Notes,
                    entry.ClinicianName, entry.EntryDate, entry.EntryTime, entry.Private, entry.InPractice));
            }

            connection.Close();
            connection.Dispose();

            return retVal;
        }

        public static List<TestSmokingDrinking> GetAllSmokingDrinkingForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestSmokingDrinking> retVal = connection.Query<TestSmokingDrinking>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                sd.ClinicalTerm, sd.ClinicalCode, sd.Status, sd.Value1, sd.Value2, sd.Value3, sd.StartDate, sd.StopDate, sd.Notes
                from EncounterEntry ee 
                inner join SmokingDrinking sd on ee.SmokingDrinkingID = sd.ID
                where ee.EncounterID = ? order by ee.Sequence;", encounterId);

            foreach (TestSmokingDrinking entry in retVal)
            {
                if (!string.IsNullOrEmpty(entry.ClinicianName))
                {
                    entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
                }

                Debug.WriteLine(string.Format("SmokingDrinking entry retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}",
                    encounterId, entry.QuickListSelection, entry.ClinicalTerm, entry.ClinicalCode, entry.Status, entry.Value1, entry.Value2, entry.Value3, 
                    entry.StartDate, entry.StopDate, entry.Notes, entry.ClinicianName, entry.EntryDate, entry.EntryTime,
                    entry.InPractice, entry.Private));
            }

            connection.Close();
            connection.Dispose();

            return retVal;
        }

        public static List<TestTestResult> GetAllTestResultsForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestTestResult> retVal = connection.Query<TestTestResult>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                r.SearchTerm, r.ClinicalTerm, r.ClinicalCode, r.Result, r.Operator, r.Unit, r.ReadOnlyUnit, r.ResultQualifier, r.Category, r.Notes, r.Centile,
                r.ResultFieldMinValue, r.ResultFieldMaxValue
                from EncounterEntry ee 
                inner join TestResult r on ee.TestResultID = r.ID
                where ee.EncounterID = ?;", encounterId);

            foreach (TestTestResult entry in retVal)
            {
                if (!string.IsNullOrEmpty(entry.ClinicianName))
                {
                    entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
                }

                Debug.WriteLine(string.Format("TestResult retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}",
                    encounterId, entry.QuickListSelection, entry.SearchTerm, entry.ClinicalTerm, entry.ClinicalCode, entry.Result, entry.Unit, entry.ReadOnlyUnit, entry.Operator, entry.ResultQualifier, 
                    entry.Category, entry.Notes, entry.Centile, entry.ResultFieldMinValue, entry.ResultFieldMaxValue, entry.ClinicianName, entry.EntryDate, entry.EntryTime));
            }

            connection.Close();
            connection.Dispose();

            return retVal;
        }

        public static List<TestUser> GetAllUsersForTestCase(ISQLitePlatform sqlitePlatform, int testCaseId)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestUser> retVal = connection.Query<TestUser>(
                "SELECT u.ID, UserName, Password, DisplayName, DisplayNameV3 from TestCaseUserMapping join " + GetUserTableName() + " u on TestCaseUserMapping.UserID = u.ID where TestCaseID = ?;",
                testCaseId);

            foreach (TestUser user in retVal)
            {
                Debug.WriteLine("TestUser retrieved for Test Case Id {0}: {1}, {2}, {3}, {4}", testCaseId, user.UserName, user.Password, user.DisplayName, user.DisplayNameV3);
            }

            connection.Close();
            connection.Dispose();

            return retVal;
        }

        public static List<TestVisualAcuity> GetAllVisualAcuityForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestVisualAcuity> retVal = connection.Query<TestVisualAcuity>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice, 
                va.ClinicalCode, va.ClinicalTerm, va.VisualAcuity, va.VisualAid, va.Notes
                from EncounterEntry ee 
                inner join VisualAcuity va on ee.VisualAcuityID = va.ID
                where ee.EncounterID = ?;", encounterId);

            foreach (TestVisualAcuity entry in retVal)
            {
                if (!string.IsNullOrEmpty(entry.ClinicianName))
                {
                    entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
                }

                Debug.WriteLine(string.Format("VisualAcuity retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}",
                    encounterId, entry.QuickListSelection, entry.ClinicalCode, entry.ClinicalTerm, entry.VisualAcuity, entry.VisualAid, entry.Notes,
                    entry.ClinicianName, entry.EntryDate, entry.EntryTime, entry.Private, entry.InPractice));
            }

            connection.Close();
            connection.Dispose();

            return retVal;
        }

        public static List<TestCorrespondence> GetAllCorrespondenceForPatient(ISQLitePlatform sqlitePlatform, string patientId)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestCorrespondence> retVal = connection.Query<TestCorrespondence>(
                @"SELECT c.EntryDate, c.CorrespondenceType, c.DocumentType, c.NameOfFile, c.Notes, c.ClinicianName
                from Correspondence c
                where c.PatientID = ?;", patientId);

            foreach (TestCorrespondence entry in retVal)
            {
                Debug.WriteLine(string.Format("Correspondence entries retrieved for Patient Id {0}: {1}, {2}, {3}, {4}, {5}, {6}",
                    patientId, entry.EntryDate, entry.CorrespondenceType, entry.DocumentType,
                    entry.NameOfFile, entry.Notes, entry.ClinicianName));
            }

            connection.Close();
            connection.Dispose();

            return retVal;
        }

        public static TestAllergy GetAllergyForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestAllergy> retVal = connection.Query<TestAllergy>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice, a.ReactionClinicalTerm, a.ReactionClinicalCode, 
                a.ClinicalTermSearchTerm, a.ClinicalTerm, a.ClinicalCode, a.DrugNameSearchTerm, a.DrugName, a.DrugCode, a.ReactionType, a.Certainty, a.Severity, 
                a.AllergyDescription, a.Notes  
                from EncounterEntry ee 
                inner join Allergy a on ee.AllergyID = a.ID
                where ee.EncounterID = ?;", encounterId);

            TestAllergy entry = retVal[0];

            if (!string.IsNullOrEmpty(entry.ClinicianName))
            {
                entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
            }

            connection.Close();
            connection.Dispose();

            Debug.WriteLine(
                string.Format(
                    "Allergy retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}",
                    encounterId, entry.QuickListSelection, entry.ClinicalTermSearchTerm, entry.ClinicalTerm, entry.ClinicalCode,
                    entry.DrugNameSearchTerm, entry.DrugName, entry.DrugCode,
                    entry.ReactionType, entry.Certainty, entry.Severity, entry.ReactionClinicalTerm,
                    entry.ReactionClinicalCode, entry.Notes, entry.ClinicianName, entry.EntryDate, entry.EntryTime));

            return entry;
        }

        public static TestAnkleNeuropathy GetAnkleNeuropathyForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestAnkleNeuropathy> retVal = connection.Query<TestAnkleNeuropathy>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                an.ClinicalCode, an.ClinicalTerm, an.RightJerk, an.LeftJerk, an.RightVib, an.LeftVib, an.Notes
                from EncounterEntry ee 
                inner join AnkleNeuropathy an on ee.AnkleNeuropathyID = an.ID
                where ee.EncounterID = ?;", encounterId);

            TestAnkleNeuropathy entry = retVal[0];

            if (!string.IsNullOrEmpty(entry.ClinicianName))
            {
                entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
            }

            Debug.WriteLine(string.Format("AnkleNeuropathy retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}",
                encounterId, entry.QuickListSelection, entry.ClinicalCode, entry.ClinicalTerm, entry.RightJerk, entry.LeftJerk, entry.RightVib, entry.LeftVib, entry.Notes,
                entry.ClinicianName, entry.EntryDate, entry.EntryTime, entry.Private, entry.InPractice));

            connection.Close();
            connection.Dispose();

            return entry;
        }

        public static TestBloodPressure GetBloodPressureForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestBloodPressure> retVal = connection.Query<TestBloodPressure>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                bp.ClinicalTerm, bp.ClinicalCode, bp.Systolic, bp.Diastolic, bp.Korotkoff, bp.Laterality, bp.Posture, bp.Cuff, bp.BodyText, bp.Notes
                from EncounterEntry ee 
                inner join BloodPressure bp on ee.BloodPressureId = bp.ID
                where ee.EncounterID = ? order by ee.Sequence;", encounterId);

            TestBloodPressure entry = retVal[0];

            if (!string.IsNullOrEmpty(entry.ClinicianName))
            {
                entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
            }

            Debug.WriteLine(
                string.Format(
                    "Blood Pressure retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}",
                    encounterId, entry.QuickListSelection, entry.ClinicalTerm, entry.ClinicalCode, entry.Systolic,
                    entry.Diastolic, entry.Korotkoff, entry.Laterality, entry.Posture, entry.Cuff, entry.BodyText, entry.Notes,
                    entry.ClinicianName, entry.EntryDate, entry.EntryTime,
                    entry.InPractice, entry.Private));

            connection.Close();
            connection.Dispose();

            return entry;
        }

        public static TestBreastfeedingInfo GetBreastfeedingInfoForPatient(ISQLitePlatform sqlitePlatform, int patientId)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestBreastfeedingInfo> retVal = connection.Query<TestBreastfeedingInfo>(
                @"SELECT pb.Breastfeeding from PdsBreastfeeding pb where pb.PatientID = ?;", patientId);

            TestBreastfeedingInfo entry = retVal[0];
            Debug.WriteLine(string.Format("Breastfeeding Info retrieved for PatientId {0}: Breastfeeding flag = {1}", patientId, entry.Breastfeeding));

            connection.Close();
            connection.Dispose();

            return entry;
        }

        public static TestContraception GetContraceptionForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestContraception> retVal = connection.Query<TestContraception>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice, 
                c.ClinicalCode, c.ClinicalTerm, c.ServiceType, c.IUCDate, c.Notes
                from EncounterEntry ee 
                inner join Contraception c on ee.ContraceptionID = c.ID
                where ee.EncounterID = ?;", encounterId);

            TestContraception entry = retVal[0];

            if (!string.IsNullOrEmpty(entry.ClinicianName))
            {
                entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
            }

            Debug.WriteLine(string.Format("Contraception retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}",
                encounterId, entry.QuickListSelection, entry.ClinicalCode, entry.ClinicalTerm, entry.ServiceType, entry.IUCDate, entry.Notes,
                entry.ClinicianName, entry.EntryDate, entry.EntryTime, entry.Private, entry.InPractice));

            connection.Close();
            connection.Dispose();

            return entry;
        }

        public static TestEncounter GetEncounterForPatient(ISQLitePlatform sqlitePlatform, int patientId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestEncounter> retVal = connection.Query<TestEncounter>(
                "SELECT ID, EncounterType, EncounterCode, StartDate, StartTime, EndTime, ClinicianName, Sequence from Encounter where PatientID = ?;", patientId);

            TestEncounter encounter = retVal[0];

            if (!string.IsNullOrEmpty(encounter.ClinicianName))
            {
                encounter.ClinicianName = encounter.ClinicianName.Replace("@RegisteredGP", registeredGp);
            }

            Debug.WriteLine(string.Format("Encounter retrieved for patientId {0}: {1}, {2}, {3}, {4}, {5}, {6}", patientId, encounter.ID, encounter.EncounterType, encounter.EncounterCode, encounter.StartDate, encounter.StartTime, encounter.EndTime));

            connection.Close();
            connection.Dispose();

            return encounter;
        }

        public static TestEncounter GetEncounterForTestCase(ISQLitePlatform sqlitePlatform, int testCaseId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestEncounter> retVal = connection.Query<TestEncounter>(
                "SELECT ID, EncounterType, EncounterCode, StartDate, StartTime, EndTime, ClinicianName, Sequence from Encounter where TestCaseID = ?;", testCaseId);

            if (retVal.Count == 0)
            {
                throw new Exception(string.Format("No Encounter linked to test case {0}", testCaseId));
            }

            TestEncounter encounter = retVal[0];

            if (!string.IsNullOrEmpty(encounter.ClinicianName))
            {
                encounter.ClinicianName = encounter.ClinicianName.Replace("@RegisteredGP", registeredGp);
            }

            Debug.WriteLine(string.Format("Encounter retrieved for TestCaseID {0}: {1}, {2}, {3}, {4}, {5}, {6}", testCaseId, encounter.ID, encounter.EncounterType, encounter.EncounterCode, encounter.StartDate, encounter.StartTime, encounter.EndTime));

            connection.Close();
            connection.Dispose();

            return encounter;
        }

        public static TestExamination GetExaminationForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestExamination> retVal = connection.Query<TestExamination>(
               @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence,  ee.Private, ee.InPractice,
                e.ClinicalTerm, e.ClinicalCode, e.Value1, e.Notes
                from EncounterEntry ee 
                inner join Examination e on ee.ExaminationID = e.ID
                where ee.EncounterID = ?;", encounterId);

            TestExamination entry = retVal[0];

            if (!string.IsNullOrEmpty(entry.ClinicianName))
            {
                entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
            }

            Debug.WriteLine(string.Format("Examination retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}",
                encounterId, entry.QuickListSelection, entry.ClinicalTerm, entry.ClinicalCode, entry.Value1, 
                entry.Notes, entry.ClinicianName, entry.EntryDate, entry.EntryTime, entry.InPractice, entry.Private));

            connection.Close();
            connection.Dispose();

            return entry;
        }

        public static TestFootPulse GetFootPulseForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestFootPulse> retVal = connection.Query<TestFootPulse>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                fp.ClinicalCode, fp.ClinicalTerm, fp.DP, fp.PT, fp.Notes
                from EncounterEntry ee 
                inner join FootPulse fp on ee.FootPulseID = fp.ID
                where ee.EncounterID = ?;", encounterId);

            TestFootPulse entry = retVal[0];

            if (!string.IsNullOrEmpty(entry.ClinicianName))
            {
                entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
            }

            Debug.WriteLine(string.Format("FootPulse retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}",
                encounterId, entry.QuickListSelection, entry.ClinicalCode, entry.ClinicalTerm, entry.DP, entry.PT, entry.Notes,
                entry.Private, entry.InPractice, entry.ClinicianName, entry.EntryDate, entry.EntryTime));

            connection.Close();
            connection.Dispose();

            return entry;
        }

        public static TestGeneralEntry GetGeneralEntryForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestGeneralEntry> retVal = connection.Query<TestGeneralEntry>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice, ge.SearchTerm, 
                ge.ClinicalTerm, ge.ClinicalCode, ge.Priority, ge.EpisodeType, ge.CategoryBeforeWriteBack, ge.Category, ge.Characteristic, ge.EndDate, ge.Notes, 
                ge.Value, ge.Unit 
                from EncounterEntry ee 
                inner join GeneralEntry ge on ee.GeneralEntryID = ge.ID
                where ee.EncounterID = ?;", encounterId);

            TestGeneralEntry entry = retVal[0];

            if (!string.IsNullOrEmpty(entry.ClinicianName))
            {
                entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
            }

            Debug.WriteLine(string.Format("General Entry retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}",
                encounterId, entry.QuickListSelection, entry.SearchTerm, entry.ClinicalTerm, entry.ClinicalCode, entry.Notes, entry.Priority, entry.EpisodeType,
                entry.CategoryBeforeWriteBack, entry.Category, entry.Characteristic, entry.Private, entry.InPractice, entry.ClinicianName, entry.EntryDate, entry.EntryTime, 
                entry.Value, entry.Unit));

            connection.Close();
            connection.Dispose();

            return entry;
        }

        public static TestHealthBoard GetHealthBoardForTestCase(ISQLitePlatform sqlitePlatform, int testCaseId)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestHealthBoard> retVal = connection.Query<TestHealthBoard>(
                "SELECT HealthBoard.ID, HealthBoardName, Region, Code from TestCaseHealthBoardMapping join HealthBoard on TestCaseHealthBoardMapping.HealthBoardID = HealthBoard.ID where TestCaseID = ?;", testCaseId);

            // Default user is UserId 1 = NHS England
            if (retVal.Count == 0)
            {
                retVal = connection.Query<TestHealthBoard>("SELECT HealthBoard.ID, HealthBoardName, Region, Code from HealthBoard where HealthBoard.ID = 1");
            }

            connection.Close();
            connection.Dispose();

            TestHealthBoard healthBoard = retVal[0];
            Debug.WriteLine(string.Format("Healthboard retrieved for TestCaseId {0}: {1}, {2}, {3}",
                testCaseId, healthBoard.Region, healthBoard.HealthBoardName, healthBoard.Code));

            return healthBoard;
        }

        public static TestHeartExamination GetHeartExaminationForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestHeartExamination> retVal = connection.Query<TestHeartExamination>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                he.ClinicalCode, he.ClinicalTerm, he.Rhythmn, he.Size, he.Sounds, he.Notes
                from EncounterEntry ee 
                inner join HeartExamination he on ee.HeartExaminationID = he.ID
                where ee.EncounterID = ?;", encounterId);

            TestHeartExamination entry = retVal[0];

            if (!string.IsNullOrEmpty(entry.ClinicianName))
            {
                entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
            }

            Debug.WriteLine(string.Format("HeartExamination retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}",
                encounterId, entry.QuickListSelection, entry.ClinicalCode, entry.ClinicalTerm, entry.Rhythmn, entry.Size, entry.Sounds, entry.Notes,
                entry.ClinicianName, entry.EntryDate, entry.EntryTime, entry.Private, entry.InPractice));

            connection.Close();
            connection.Dispose();

            return entry;
        }

        public static TestImmunisation GetImmunisationForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestImmunisation> retVal = connection.Query<TestImmunisation>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                i.SearchTerm, i.ImmunisationType, i.ClinicalTerm, i.ClinicalCode, i.Source, i.BatchNo, i.Method, i.Site, i.Reason, i.ReasonText, 
                i.DueDate, i.Notes, i.Stage, i.StageValue, i.Status 
                from EncounterEntry ee 
                inner join Immunisation i on ee.ImmunisationId = i.ID
                where ee.EncounterID = ?;", encounterId);

            TestImmunisation entry = retVal[0];

            if (!string.IsNullOrEmpty(entry.ClinicianName))
            {
                entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
            }

            Debug.WriteLine(string.Format("Immunisation retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}, {18}, {19}",
                encounterId, entry.QuickListSelection, entry.SearchTerm, entry.ImmunisationType, entry.ClinicalTerm, entry.ClinicalCode,
                entry.Stage, entry.StageValue, entry.Status, entry.Source, entry.BatchNo, entry.Method, entry.Site, entry.Reason, entry.ReasonText, entry.DueDate,
                entry.Notes, entry.ClinicianName, entry.EntryDate, entry.EntryTime));

            connection.Close();
            connection.Dispose();

            return entry;
        }

        public static TestLifestyle GetLifestyleForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestLifestyle> retVal = connection.Query<TestLifestyle>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                l.ClinicalCode, l.ClinicalTerm, l.Type, l.Notes
                from EncounterEntry ee 
                inner join Lifestyle l on ee.LifestyleID = l.ID
                where ee.EncounterID = ?;", encounterId);

            TestLifestyle entry = retVal[0];

            if (!string.IsNullOrEmpty(entry.ClinicianName))
            {
                entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
            }

            Debug.WriteLine(string.Format("Lifestyle retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}",
                encounterId, entry.QuickListSelection, entry.ClinicalCode, entry.ClinicalTerm, entry.Type, entry.Notes,
                entry.ClinicianName, entry.EntryDate, entry.EntryTime, entry.Private, entry.InPractice));

            connection.Close();
            connection.Dispose();

            return entry;
        }

        public static List<TestMedication> GetLinkedMedications(ISQLitePlatform sqlitePlatform, int parentEntryId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestMedication> retVal = connection.Query<TestMedication>(
                @"SELECT ee.ID, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Private, ee.InPractice,
                m.MedicationType, m.DrugName, m.Code, m.Quantity, m.QuantityDefault, m.DrugClassDefault, m.DrugClass, 
                m.DosageInstructionsDefault, m.DosageInstructions, m.PreparationDefault, m.Preparation, 
                m.SearchTerm, m.BrowseFolders, m.NumberOfRepeats, m.IssueNumber, m.TotalIssues, m.PrescribedDate, m.LastIssueDate, m.RepeatUntilDate,
                m.IsGeneric, m.IsControlled, m.IsHighRisk, m.IsDiscontinued, m.EligibleForIssue, m.EligibleForReauth, 
                m.IssueWarnings, m.ReauthWarnings
                from Medication m inner join EncounterEntry ee
                on m.id = ee.MedicationId where ee.ParentEntryId =  ? order by ee.Sequence desc;", parentEntryId);

            foreach (TestMedication medication in retVal)
            {
                if (!string.IsNullOrEmpty(medication.ClinicianName))
                {
                    medication.ClinicianName = medication.ClinicianName.Replace("@RegisteredGP", registeredGp);
                }

                Debug.WriteLine(string.Format("Linked Medication retrieved for ParentEntryId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}, {18}",
                    parentEntryId, medication.MedicationType, medication.DrugName, medication.Code, medication.DrugClassDefault, medication.DrugClass,
                    medication.DosageInstructionsDefault, medication.DosageInstructions, medication.QuantityDefault, medication.Quantity,
                    medication.PreparationDefault, medication.Preparation, medication.RepeatUntilDate, medication.NumberOfRepeats,
                    medication.IssueNumber, medication.TotalIssues, medication.ClinicianName, medication.EntryDate, medication.EntryTime));
            }

            connection.Close();
            connection.Dispose();

            return retVal;
        }

        public static TestMedicalHistory GetMedicalHistoryForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestMedicalHistory> retVal = connection.Query<TestMedicalHistory>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                mh.ClinicalCode, mh.ClinicalTerm, mh.ClinicalCode2, mh.ClinicalTerm2, mh.Notes
                from EncounterEntry ee 
                inner join MedicalHistory mh on ee.MedicalHistoryID = mh.ID
                where ee.EncounterID = ?;", encounterId);

            TestMedicalHistory entry = retVal[0];

            if (!string.IsNullOrEmpty(entry.ClinicianName))
            {
                entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
            }

            Debug.WriteLine(string.Format("MedicalHistory retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}",
                encounterId, entry.QuickListSelection, entry.ClinicalCode, entry.ClinicalTerm, entry.ClinicalCode2, entry.ClinicalTerm2, entry.Notes,
                entry.ClinicianName, entry.EntryDate, entry.EntryTime, entry.Private, entry.InPractice));

            connection.Close();
            connection.Dispose();

            return entry;
        }

        public static TestMedication GetMedicationForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());

            List<TestMedication> retVal = connection.Query<TestMedication>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                m.MedicationType, m.DrugName, m.Code, m.Quantity, m.QuantityDefault, m.AvailablePackSizes, m.PackSize, m.DrugClassDefault, m.DrugClass, 
                m.DosageInstructionsDefault, m.DosageInstructions, m.StandardDosageDefault, m.StandardDosage, m.UserDefinedDosage, m.PreparationDefault, m.Preparation, 
                m.SearchTerm, m.BrowseFolders, m.NumberOfRepeats, m.IssueNumber, m.TotalIssues, m.PrescribedDate, m.LastIssueDate, m.RepeatUntilDate, 
                m.IsGeneric, m.IsControlled, m.IsHighRisk, m.IsDiscontinued, m.EligibleForIssue, m.EligibleForReauth,
                m.IssueWarnings, m.ReauthWarnings, m.HighRiskWarning
                from EncounterEntry ee inner join Medication m on ee.MedicationID = m.ID where ee.EncounterID = ?;", encounterId);

            TestMedication medication = retVal[0];

            if (!string.IsNullOrEmpty(medication.ClinicianName))
            {
                medication.ClinicianName = medication.ClinicianName.Replace("@RegisteredGP", registeredGp);
            }

            Debug.WriteLine(string.Format("Medication retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}, {18}, {19}, {20}, {21}, {22}, {23}, {24}, {25}, {26}", 
                encounterId, medication.SearchTerm,
                medication.BrowseFolders, medication.MedicationType, medication.DrugName, medication.Code, medication.DrugClassDefault, medication.DrugClass,
                medication.DosageInstructionsDefault, medication.DosageInstructions, medication.StandardDosageDefault, medication.StandardDosage, medication.UserDefinedDosage,
                medication.QuantityDefault, medication.Quantity, medication.AvailablePackSizes, medication.PackSize,
                medication.PreparationDefault, medication.Preparation, medication.RepeatUntilDate, medication.NumberOfRepeats,
                medication.IssueNumber, medication.TotalIssues, medication.ClinicianName, medication.EntryDate, medication.EntryTime, medication.HighRiskWarning));

            connection.Close();
            connection.Dispose();

            return medication;
        }

        public static TestPassiveSmoking GetPassiveSmokingForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestPassiveSmoking> retVal = connection.Query<TestPassiveSmoking>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                ps.ClinicalCode, ps.ClinicalTerm, ps.PassiveSmoking, ps.Cause, ps.Notes
                from EncounterEntry ee 
                inner join PassiveSmoking ps on ee.PassiveSmokingID = ps.ID
                where ee.EncounterID = ?;", encounterId);

            TestPassiveSmoking entry = retVal[0];

            if (!string.IsNullOrEmpty(entry.ClinicianName))
            {
                entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
            }

            Debug.WriteLine(string.Format("PassiveSmoking retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}",
                encounterId, entry.QuickListSelection, entry.ClinicalCode, entry.ClinicalTerm, entry.PassiveSmoking, entry.Cause, entry.Notes,
                entry.ClinicianName, entry.EntryDate, entry.EntryTime, entry.Private, entry.InPractice));

            connection.Close();
            connection.Dispose();

            return entry;
        }

        public static TestPatient GetPatientForTestCase(ISQLitePlatform sqlitePlatform, int testCaseId)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());

            string patientId = connection.ExecuteScalar<string>(
                "SELECT PatientID from TestCasePatientMapping where TestCaseID = ?;", testCaseId);

            List<TestPatient> retVal = new List<TestPatient>();

            if (patientId == "@AnyLeeds")
            {
                // Pick a random patient in the Leeds range of patient
                int id = randomPatientId.Next(7000, 10103);
                Debug.WriteLine("RANDOM PATIENT SELECTED FROM LEEDS PRACTICE: patient ID = " + id);
                retVal = connection.Query<TestPatient>(
                    "SELECT p.ID, p.FamilyName, p.GivenName, p.Title, p.Identifier, p.IdentifierType, p.DateOfBirth, p.Gender, p.RegisteredGP, p.UsualGP, p.Postcode, p.PracticeId, pr.PracticeName, pr.ShortPracticeName from "
                    + GetPatientTableName() + " p inner join " + GetPracticeTableName() + " pr on p.PracticeId = pr.ID where p.ID = ?;", id);
            }
            else
            {
                retVal = connection.Query<TestPatient>(
                    "SELECT p.ID, p.FamilyName, p.GivenName, p.Title, p.Identifier, p.IdentifierType, p.DateOfBirth, p.Gender, p.RegisteredGP, p.UsualGP, p.Postcode, p.PracticeId, pr.PracticeName, pr.ShortPracticeName, Sequence from TestCasePatientMapping inner join "
                + GetPatientTableName() + " p on PatientID = p.ID inner join " + GetPracticeTableName() + " pr on p.PracticeId = pr.ID where TestCaseID = ?;", testCaseId);
            }

            connection.Close();
            connection.Dispose();

            if (retVal.Count == 0)
            {
                throw new Exception("No Patient data found for Test Case " + testCaseId.ToString());
            }

            TestPatient patient = retVal[0];
            Debug.WriteLine(string.Format("Patient retrieved for test case {0}: {1}, {2} ({3}), {4}, {5}, {7}={6}, {8}, {9}, {10}, {11}, {12}", testCaseId, patient.FamilyName,
                patient.GivenName, patient.Title, patient.DateOfBirth, patient.Gender, patient.Identifier, patient.IdentifierType, patient.RegisteredGP, patient.UsualGP, patient.Postcode, patient.PracticeName, patient.ShortPracticeName));
            return patient;
        }

        public static TestPeakFlow GetPeakFlowForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestPeakFlow> retVal = connection.Query<TestPeakFlow>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                pf.ClinicalTerm, pf.ClinicalCode, pf.Result, pf.Unit, pf.Operator, pf.ResultQualifier, pf.WarningFlag, pf.DeviceType, pf.Notes
                from EncounterEntry ee 
                inner join PeakFlow pf on ee.PeakFlowID = pf.ID
                where ee.EncounterID = ?;", encounterId);

            TestPeakFlow entry = retVal[0];

            if (!string.IsNullOrEmpty(entry.ClinicianName))
            {
                entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
            }

            Debug.WriteLine(string.Format("PeakFlow retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}",
                encounterId, entry.QuickListSelection, entry.ClinicalTerm, entry.ClinicalCode, entry.Result, entry.Unit, entry.ResultQualifier, entry.WarningFlag,
                entry.DeviceType, entry.Notes, entry.ClinicianName, entry.EntryDate, entry.EntryTime));

            connection.Close();
            connection.Dispose();

            return entry;
        }

        public static TestPractice GetPracticeForTestCase(ISQLitePlatform sqlitePlatform, int testCaseId)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestPractice> retVal = connection.Query<TestPractice>(
                "SELECT p.ID, PracticeName, ShortPracticeName, HealthBoard, IsFederatedPractice from TestCasePracticeMapping join " + GetPracticeTableName() + " p on TestCasePracticeMapping.PracticeID = p.ID where TestCaseID = ?;",
                testCaseId);

            // Default user is PracticeId 1 = MA Dev Demo 01  /  INPS Dundee
            if (retVal.Count == 0)
            {
                retVal =
                    connection.Query<TestPractice>("SELECT p.ID, PracticeName, ShortPracticeName, HealthBoard, IsFederatedPractice from TestCasePracticeMapping join " + GetPracticeTableName() + " p where p.ID = 1");
            }

            connection.Close();
            connection.Dispose();

            TestPractice practice = retVal[0];
            Debug.WriteLine(string.Format("TestPractice retrieved for Test Case Id {0}: {1}, {2}, {3}, {4}", testCaseId,
                practice.PracticeName, practice.ShortPracticeName, practice.HealthBoard, practice.IsFederatedPractice));

            return practice;
        }

        public static TestPregnancyInfo GetPregnancyInfoForPatient(ISQLitePlatform sqlitePlatform, string patientId)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestPregnancyInfo> retVal = connection.Query<TestPregnancyInfo>(
                @"SELECT pp.Pregnant, pp.NoOfWeeks from PdsPregnancy pp where pp.PatientID = ?;", patientId);

            TestPregnancyInfo entry = retVal[0];
            Debug.WriteLine(string.Format("Pregnancy Info retrieved for PatientId {0}: Pregnancy flag = {1}, {2} weeks pregnant", patientId, entry.Pregnant, entry.NoOfWeeks));

            connection.Close();
            connection.Dispose();

            return entry;
        }

        public static TestProblem GetProblemForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestProblem> retVal = connection.Query<TestProblem>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                pr.ClinicalTerm, pr.ClinicalCode, pr.LinkedEntriesCount
                from EncounterEntry ee 
                inner join Problem pr on ee.ProblemID = pr.ID
                where ee.EncounterID = ?;", encounterId);

            TestProblem entry = retVal[0];

            if (!string.IsNullOrEmpty(entry.ClinicianName))
            {
                entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
            }

            Debug.WriteLine(string.Format("Problem retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}",
                encounterId, entry.QuickListSelection, entry.ClinicalTerm, entry.ClinicalCode, entry.ClinicianName, entry.EntryDate, entry.EntryTime, entry.LinkedEntriesCount));

            connection.Close();
            connection.Dispose();

            return entry;
        }

        public static TestReferral GetReferralForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestReferral> retVal = connection.Query<TestReferral>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                r.ClinicalCode, r.ClinicalTerm, r.Source, r.ReferralType, r.Attendance, r.Urgency, r.ActionDate, r.Notes
                from EncounterEntry ee 
                inner join Referral r on ee.ReferralID = r.ID
                where ee.EncounterID = ?;", encounterId);

            TestReferral entry = retVal[0];

            if (!string.IsNullOrEmpty(entry.ClinicianName))
            {
                entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
            }

            Debug.WriteLine(string.Format("Referral retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}",
                encounterId, entry.QuickListSelection, entry.ClinicalCode, entry.ClinicalTerm, entry.Source, entry.ReferralType, entry.Attendance, entry.Urgency, entry.ActionDate, entry.Notes,
                entry.ClinicianName, entry.EntryDate, entry.EntryTime, entry.Private, entry.InPractice));

            connection.Close();
            connection.Dispose();

            return entry;
        }

        public static TestRequest GetRequestForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestRequest> retVal = connection.Query<TestRequest>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                r.ClinicalCode, r.ClinicalTerm, r.Urgency, r.ActionDate, r.TestRequestSystem, r.Notes
                from EncounterEntry ee 
                inner join Request r on ee.RequestID = r.ID
                where ee.EncounterID = ?;", encounterId);

            TestRequest entry = retVal[0];

            if (!string.IsNullOrEmpty(entry.ClinicianName))
            {
                entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
            }

            Debug.WriteLine(string.Format("Request retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}",
                encounterId, entry.QuickListSelection, entry.ClinicalCode, entry.ClinicalTerm, entry.Urgency, entry.ActionDate, entry.TestRequestSystem, entry.Notes,
                entry.ClinicianName, entry.EntryDate, entry.EntryTime, entry.Private, entry.InPractice));

            connection.Close();
            connection.Dispose();

            return entry;
        }

        public static TestSmokingDrinking GetSmokingDrinkingForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestSmokingDrinking> retVal = connection.Query<TestSmokingDrinking>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                sd.ClinicalTerm, sd.ClinicalCode, sd.Status, sd.Value1, sd.Value2, sd.Value3, sd.StartDate, sd.StopDate, sd.Notes
                from EncounterEntry ee 
                inner join SmokingDrinking sd on ee.SmokingDrinkingID = sd.ID
                where ee.EncounterID = ? order by ee.Sequence;", encounterId);

            TestSmokingDrinking entry = retVal[0];

            if (!string.IsNullOrEmpty(entry.ClinicianName))
                {
                entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
                }

                Debug.WriteLine(string.Format("SmokingDrinking entry retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}",
                    encounterId, entry.QuickListSelection, entry.ClinicalTerm, entry.ClinicalCode, entry.Status, entry.Value1, entry.Value2, entry.Value3,
                    entry.StartDate, entry.StopDate, entry.Notes, entry.ClinicianName, entry.EntryDate, entry.EntryTime,
                    entry.InPractice, entry.Private));

            connection.Close();
            connection.Dispose();

            return entry;
        }

        public static TestTestResult GetTestResultForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestTestResult> retVal = connection.Query<TestTestResult>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice,
                tr.SearchTerm, tr.ClinicalTerm, tr.ClinicalCode, tr.Result, tr.Operator, tr.Unit, tr.ReadOnlyUnit, tr.ResultQualifier, tr.Laterality, tr.Category, tr.Notes, tr.Centile,
                tr.ResultFieldMinValue, tr.ResultFieldMaxValue
                from EncounterEntry ee 
                inner join TestResult tr on ee.TestResultID = tr.ID
                where ee.EncounterID = ?;", encounterId);

            TestTestResult entry = retVal[0];

            if (!string.IsNullOrEmpty(entry.ClinicianName))
            {
                entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
            }

            Debug.WriteLine(string.Format("TestResult retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}, {18}",
                encounterId, entry.QuickListSelection, entry.SearchTerm, entry.ClinicalTerm, entry.ClinicalCode, entry.Result, entry.Operator, entry.Unit, entry.ReadOnlyUnit, 
                entry.ResultQualifier, entry.Laterality, entry.Category, entry.Notes, entry.Centile, entry.ResultFieldMinValue, entry.ResultFieldMaxValue,
                entry.ClinicianName, entry.EntryDate, entry.EntryTime));

            connection.Close();
            connection.Dispose();

            return entry;
        }

        public static TestUser GetUserForTestCase(ISQLitePlatform sqlitePlatform, int testCaseId, int practiceId = 1)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestUser> retVal = connection.Query<TestUser>(
                "SELECT u.ID, UserName, Password, DisplayName, DisplayNameV3 from TestCaseUserMapping join " + GetUserTableName() + " u on TestCaseUserMapping.UserID = u.ID where TestCaseID = ?;",
                testCaseId);

            // Default user is UserId 1 for Dundee (practice id 1), UserId 6 for Leeds (practice id 2)
            if (retVal.Count == 0)
            {
                if (practiceId == 1)
                {
                    retVal = connection.Query<TestUser>("SELECT ID, UserName, Password, DisplayName, DisplayNameV3 from " + GetUserTableName() + " u where u.ID = 1");
                }
                else
                {
                    retVal = connection.Query<TestUser>("SELECT ID, UserName, Password, DisplayName, DisplayNameV3 from " + GetUserTableName() + " u where u.ID = 6");
                }
            }

            connection.Close();
            connection.Dispose();

            TestUser user = retVal[0];
            Debug.WriteLine("TestUser retrieved for Test Case Id {0}: {1}, {2}, {3}, {4}", testCaseId,
                user.UserName, user.Password, user.DisplayName, user.DisplayNameV3);

            return user;
        }

        public static TestVisualAcuity GetVisualAcuityForEncounter(ISQLitePlatform sqlitePlatform, int encounterId, string registeredGp)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestVisualAcuity> retVal = connection.Query<TestVisualAcuity>(
                @"SELECT ee.ID, ee.QuickListSelection, ee.EntryDate, ee.EntryTime, ee.ClinicianName, ee.Sequence, ee.Private, ee.InPractice, 
                va.ClinicalCode, va.ClinicalTerm, va.VisualAcuity, va.VisualAid, va.Notes
                from EncounterEntry ee 
                inner join VisualAcuity va on ee.VisualAcuityID = va.ID
                where ee.EncounterID = ?;", encounterId);

            TestVisualAcuity entry = retVal[0];

            if (!string.IsNullOrEmpty(entry.ClinicianName))
            {
                entry.ClinicianName = entry.ClinicianName.Replace("@RegisteredGP", registeredGp);
            }

            Debug.WriteLine(string.Format("VisualAcuity retrieved for EncounterId {0}: {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}",
                encounterId, entry.QuickListSelection, entry.ClinicalCode, entry.ClinicalTerm, entry.VisualAcuity, entry.VisualAid, entry.Notes,
                entry.ClinicianName, entry.EntryDate, entry.EntryTime, entry.Private, entry.InPractice));

            connection.Close();
            connection.Dispose();

            return entry;
        }

        public static TestCorrespondence GetCorrespondenceForPatient(ISQLitePlatform sqlitePlatform, string patientId)
        {
            SQLiteConnection connection = new SQLiteConnection(sqlitePlatform, GetTestDatabaseFullPath());
            List<TestCorrespondence> retVal = connection.Query<TestCorrespondence>(
                 @"SELECT c.EntryDate, c.CorrespondenceType, c.DocumentType, c.NameOfFile, c.Notes, c.ClinicianName
                from Correspondence c
                where c.PatientID = ?;", patientId);

            TestCorrespondence entry = retVal[0];

            Debug.WriteLine(string.Format("Correspondence entry for Patient Id {0}: {1}, {2}, {3}, {4}, {5}, {6}",
                patientId, entry.EntryDate, entry.CorrespondenceType, entry.DocumentType,
                entry.NameOfFile, entry.Notes, entry.ClinicianName));

            connection.Close();
            connection.Dispose();

            return entry;
        }

        private static string GetTestDatabaseFullPath()
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "PHFTest.db");
        }

        private static string GetPatientTableName()
        {
#if BACKEND_PRODUCTION
            return "PatientDevTest";
#else
            return "PatientVIAB";
#endif
        }

        private static string GetUserTableName()
        {
#if BACKEND_PRODUCTION
            return "UserProduction";
#elif BACKEND_VIAB_USEVISIONPRACTICE
            return "UserVIAB_CR17";
#else
            return "UserVIAB";
#endif
        }

        private static string GetPracticeTableName()
        {
#if BACKEND_PRODUCTION
            return "PracticeProduction";
#elif BACKEND_VIAB_USEVISIONPRACTICE
            return "PracticeVIAB_CR17";
#else
            return "PracticeVIAB";
#endif
        }
    }
}
