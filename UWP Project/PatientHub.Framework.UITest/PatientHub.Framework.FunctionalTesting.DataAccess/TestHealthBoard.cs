﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using SQLite.Net.Attributes;

    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class TestHealthBoard
    {

        /// <summary>
        /// Gets or sets the internal database Id.
        /// </summary>
        [AutoIncrement, PrimaryKey]
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the Health Board Name
        /// </summary>
        public string HealthBoardName { get; set; }

        /// <summary>
        /// Gets or sets the Region to which the healthboard belongs
        /// </summary>
        public string Region { get; set; }

        /// <summary>
        /// Gets or sets the Health Board Code 
        /// </summary>
        public string Code { get; set; }
    }
}
