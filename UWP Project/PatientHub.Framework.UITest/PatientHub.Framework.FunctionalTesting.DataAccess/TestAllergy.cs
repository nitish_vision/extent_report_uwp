﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using SQLite.Net.Attributes;

    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class TestAllergy : TestEntryBase
    {
        /// <summary>
        /// *** Do not use - replaced with DrugNameSearchTerm ***
        /// Gets or sets the Drug Name Search Term.
        /// </summary>
        public string SearchTerm
        {
            get
            {
                return DrugNameSearchTerm;
            }
            set
            {
                DrugNameSearchTerm = value;
            }
        }

        /// <summary>
        /// Gets or sets the Drug Name Search Term.
        /// </summary>
        public string DrugNameSearchTerm { get; set; }

        /// <summary>
        /// Gets or sets the Clinical Term Search Term.
        /// </summary>
        public string ClinicalTermSearchTerm { get; set; }

        /// <summary>
        /// Gets or sets the Drug Name.
        /// </summary>
        public string DrugName { get; set; }

        /// <summary>
        /// Gets or sets the Drug Code.
        /// </summary>
        public string DrugCode { get; set; }

        /// <summary>
        /// Gets or sets the Reaction Type.
        /// </summary>
        public string ReactionType { get; set; }

        /// <summary>
        /// Gets or sets the Certainty.
        /// </summary>
        public string Certainty { get; set; }

        /// <summary>
        /// Gets or sets the Severity.
        /// </summary>
        public string Severity { get; set; }

        /// <summary>
        /// Gets or sets the AllergyDescription.
        /// </summary>
        public string AllergyDescription { get; set; }

        /// <summary>
        /// Gets or sets the Reaction Clinical Term.
        /// </summary>
        public string ReactionClinicalTerm { get; set; }

        /// <summary>
        /// Gets or sets the Reaction Clinical Code.
        /// </summary>
        public string ReactionClinicalCode { get; set; }

    }
}
