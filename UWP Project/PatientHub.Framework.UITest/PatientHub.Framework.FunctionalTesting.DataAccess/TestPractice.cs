﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using SQLite.Net.Attributes;

    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class TestPractice
    {

        /// <summary>
        /// Gets or sets the internal database Id.
        /// </summary>
        [AutoIncrement, PrimaryKey]
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the Practice Name
        /// </summary>
        public string PracticeName { get; set; }

        /// <summary>
        /// Gets or sets the ShortPractice Name
        /// </summary>
        public string ShortPracticeName { get; set; }

        /// <summary>
        /// Gets or sets the Health Board to which the practice belongs
        /// </summary>
        public string HealthBoard { get; set; }

        /// <summary>
        /// Gets or sets whether it's a Federated practice or not
        /// </summary>
        public bool IsFederatedPractice { get; set; }

    }
}
