﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using SQLite.Net.Attributes;

    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class TestPatientContactInfo
    {

        /// <summary>
        /// Gets or sets the internal database Id.
        /// </summary>
        [AutoIncrement, PrimaryKey]
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the Contact Type
        /// </summary>
        public string ContactType { get; set; }

        /// <summary>
        /// Gets or sets the Contact Value
        /// </summary>
        public string ContactValue { get; set; }

        /// <summary>
        /// Gets or sets the IsAddress flag
        /// </summary>
        public bool IsAddress { get; set; }
    }
}
