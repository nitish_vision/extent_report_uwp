﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using SQLite.Net.Attributes;

    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class TestFootPulse : TestEntryBase
    {
        /// <summary>
        /// Gets or sets the DP.
        /// </summary>
        public string DP { get; set; }

        /// <summary>
        /// Gets or sets the PT.
        /// </summary>
        public string PT { get; set; }
    }
}
