﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using SQLite.Net.Attributes;

    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class TestProblem : TestEntryBase
    {
        /// <summary>
        /// Gets or sets the Category.
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// Gets or sets the LinkedEntriesCount.
        /// </summary>
        public int LinkedEntriesCount { get; set; }
    }
}
