﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using SQLite.Net.Attributes;

    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class TestLifestyle : TestEntryBase
    {
        /// <summary>
        /// Gets or sets the Type.
        /// </summary>
        public string Type { get; set; }

    }
}
