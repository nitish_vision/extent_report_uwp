﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using SQLite.Net.Attributes;

    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class DataEntrySearch
    {
        /// <summary>
        /// Gets or sets the Search Term.
        /// </summary>
        public string SearchTerm { get; set; }

        /// <summary>
        /// Gets or sets the Expected Result Count.
        /// </summary>
        public Nullable<Int32> ExpectedResultCount { get; set; }

        /// <summary>
        /// Gets or sets the ExpectedResults.
        /// </summary>
        public string ExpectedResults { get; set; }

    }
}
