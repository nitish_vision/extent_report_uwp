﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using SQLite.Net.Attributes;

    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class TestMedicalHistory : TestEntryBase
    {
        /// <summary>
        /// Gets or sets the ClinicalCode2.
        /// </summary>
        public string ClinicalCode2 { get; set; }

        /// <summary>
        /// Gets or sets the ClinicalTerm2.
        /// </summary>
        public string ClinicalTerm2 { get; set; }

    }
}
