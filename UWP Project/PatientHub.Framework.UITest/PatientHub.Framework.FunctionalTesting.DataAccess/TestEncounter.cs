﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using SQLite.Net.Attributes;

    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class TestEncounter
    {
        private string startDate;

        /// <summary>
        /// Gets or sets the internal database Id.
        /// </summary>
        [AutoIncrement, PrimaryKey]
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the Encounter Type.
        /// </summary>
        public string EncounterType { get; set; }

        /// <summary>
        /// Gets or sets the Encounter Code.
        /// </summary>
        public string EncounterCode { get; set; }

        /// <summary>
        /// Gets or sets the sequence.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Gets or sets the start datetime.
        /// </summary>
        public string StartDate 
        {
            get
            {
                return this.startDate;
            }

            set
            {
                if (value != null)
                {
                    if (value.StartsWith("@Today"))
                    {
                        this.startDate = DateTime.Today.AddDays(Int32.Parse(value.Substring(6))).ToShortDateString();
                    }
                    else
                    {
                        this.startDate = value;
                    }
                }
                else
                {
                    this.startDate = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the start time.
        /// </summary>
        public string StartTime { get; set; }

        /// <summary>
        /// Gets or sets the end time.
        /// </summary>
        public string EndTime { get; set; }

        /// <summary>
        /// Gets or sets the clinician name.
        /// </summary>
        public string ClinicianName { get; set; }
    }
}
