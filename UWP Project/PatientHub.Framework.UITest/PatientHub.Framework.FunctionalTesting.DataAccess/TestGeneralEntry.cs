﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using SQLite.Net.Attributes;

    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class TestGeneralEntry : TestEntryBase
    {
        /// <summary>
        /// Gets or sets the Search Term.
        /// </summary>
        public string SearchTerm { get; set; }

        /// <summary>
        /// Gets or sets the Priority.
        /// </summary>
        public int Priority { get; set; }

        /// <summary>
        /// Gets or sets the EpisodeType.
        /// </summary>
        public string EpisodeType { get; set; }

        /// <summary>
        /// Gets or sets the EndDate.
        /// </summary>
        public string EndDate { get; set; }

        /// <summary>
        /// Gets or sets the CategoryBeforeWriteBack.
        /// </summary>
        public string CategoryBeforeWriteBack { get; set; }

        /// <summary>
        /// Gets or sets the Category.
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// Gets or sets the Characteristic.
        /// </summary>
        public string Characteristic { get; set; }

        /// <summary>
        /// Gets or sets the Value for Value-based Entries.
        /// </summary>
        public Nullable<double> Value { get; set; }

        /// <summary>
        /// Gets or sets the lower limit of the Value field, for Value-based Entries.
        /// </summary>
        public Nullable<double> ValueFieldMinValue { get; set; }

        /// <summary>
        /// Gets or sets the upper limit of the Value field, for Value-based Entries.
        /// </summary>
        public Nullable<double> ValueFieldMaxValue { get; set; }

        /// <summary>
        /// Gets or sets the Unit.
        /// </summary>
        public string Unit { get; set; }

    }
}
