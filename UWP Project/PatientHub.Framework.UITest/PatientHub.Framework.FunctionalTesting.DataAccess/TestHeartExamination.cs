﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using SQLite.Net.Attributes;

    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class TestHeartExamination : TestEntryBase
    {
        /// <summary>
        /// Gets or sets the Rhythmn.
        /// </summary>
        public string Rhythmn { get; set; }

        /// <summary>
        /// Gets or sets the Size.
        /// </summary>
        public string Size { get; set; }

        /// <summary>
        /// Gets or sets the Sounds.
        /// </summary>
        public string Sounds { get; set; }

    }
}
