﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using SQLite.Net.Attributes;

    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class TestPassiveSmoking : TestEntryBase
    {
        /// <summary>
        /// Gets or sets the Passive Smoking.
        /// </summary>
        public string PassiveSmoking { get; set; }

        /// <summary>
        /// Gets or sets the Cause.
        /// </summary>
        public string Cause { get; set; }
    }
}
