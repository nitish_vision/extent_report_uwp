﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using SQLite.Net.Attributes;

    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class TestBreastfeedingInfo
    {

        /// <summary>
        /// Gets or sets the internal database Id.
        /// </summary>
        [AutoIncrement, PrimaryKey]
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the Breastfeeding flag
        /// </summary>
        public int Breastfeeding { get; set; }

    }
}
