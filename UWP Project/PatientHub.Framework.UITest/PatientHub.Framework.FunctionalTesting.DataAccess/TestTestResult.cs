﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using SQLite.Net.Attributes;

    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class TestTestResult : TestEntryBase
    {
        /// <summary>
        /// Gets or sets the Search Term.
        /// </summary>
        public string SearchTerm { get; set; }

        /// <summary>
        /// Gets or sets the Result.
        /// </summary>
        public Nullable<double> Result { get; set; }

        /// <summary>
        /// Gets or sets the Operator.
        /// </summary>
        public string Operator { get; set; }

        /// <summary>
        /// Gets or sets the Unit.
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// Gets or sets the ReadOnlyUnit.
        /// </summary>
        public string ReadOnlyUnit { get; set; }

        /// <summary>
        /// Gets or sets the Laterality.
        /// </summary>
        public string Laterality { get; set; }

        /// <summary>
        /// Gets or sets the Category.
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// Gets or sets the ResultQualifier.
        /// </summary>
        public string ResultQualifier { get; set; }

        /// <summary>
        /// Gets or sets the ResultFieldMinValue
        /// </summary>
        public Nullable<double> ResultFieldMinValue { get; set; }

        /// <summary>
        /// Gets or sets the ResultFieldMaxValue
        /// </summary>
        public Nullable<double> ResultFieldMaxValue { get; set; }

        /// Gets or sets the Centile.
        /// </summary>
        public string Centile { get; set; }
    }
}
