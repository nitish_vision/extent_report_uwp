﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using SQLite.Net.Attributes;

    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class TestReferral : TestEntryBase
    {
        /// <summary>
        /// Gets or sets the Source.
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        /// Gets or sets the ReferralType.
        /// </summary>
        public string ReferralType { get; set; }

        /// <summary>
        /// Gets or sets the Attendance.
        /// </summary>
        public string Attendance { get; set; }

        /// <summary>
        /// Gets or sets the Urgency.
        /// </summary>
        public string Urgency { get; set; }

        /// <summary>
        /// Gets or sets the ActionDate.
        /// </summary>
        public string ActionDate { get; set; }
    }
}
