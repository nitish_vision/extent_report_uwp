﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using SQLite.Net.Attributes;

    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class TestPeakFlow : TestEntryBase
    {
        /// <summary>
        /// Gets or sets the Operator.
        /// </summary>
        public string Operator { get; set; }

        /// <summary>
        /// Gets or sets the Result.
        /// </summary>
        public double Result { get; set; }

        /// <summary>
        /// Gets or sets the Unit.
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// Gets or sets the ResultQualifier.
        /// </summary>
        public string ResultQualifier { get; set; }

        /// <summary>
        /// Gets or sets the WarningFlag.
        /// </summary>
        public int WarningFlag { get; set; }

        /// <summary>
        /// Gets or sets the DeviceType.
        /// </summary>
        public string DeviceType { get; set; }

    }
}
