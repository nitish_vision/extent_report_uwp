select e.testcaseid, e.encountertype, e.startdate, e.starttime, e.clinicianname,
--ex.id, ex.clinicalterm, ex.clinicalcode, ex.value1, ex.notes, 
--al.id, al.clinicalterm, al.clinicalcode, 
--pf.id, pf.clinicalterm, pf.clinicalcode, pf.result, pf.unit, pf.devicetype, pf.resultqualifier, 
ge.id, ge.clinicalterm, ge.clinicalcode, ge.value, ge.notes,
tr.id, tr.searchterm, tr.clinicalterm, tr.clinicalcode, tr.result, tr.unit, tr.notes, 
ee.entrydate, ee.entrytime, ee.clinicianname, ee.quicklistselection
from encounter e 
join encounterentry ee on e.id = ee.encounterid
left join examination ex on ee.examinationid = ex.id
left join allergy al on ee.allergyid = al.id
left join PeakFlow pf on ee.peakflowid = pf.id
left join GeneralEntry ge on ee.generalentryid = ge.id 
left join TestResult tr on ee.testresultid = tr.id 
where testcaseid = '12552'
