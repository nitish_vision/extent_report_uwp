﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using SQLite.Net.Attributes;

    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class TestContraception : TestEntryBase
    {
        /// <summary>
        /// Gets or sets the Service Type.
        /// </summary>
        public string ServiceType { get; set; }

        /// <summary>
        /// Gets or sets the IUC Date.
        /// </summary>
        public string IUCDate { get; set; }

    }
}
