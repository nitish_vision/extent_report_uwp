﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using SQLite.Net.Attributes;

    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class TestEntryBase
    {
        private string entryDate;

        /// <summary>
        /// Gets or sets the internal database Id.
        /// </summary>
        [AutoIncrement, PrimaryKey]
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the sequence.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Gets or sets the ClinicalCode.
        /// </summary>
        public string ClinicalCode { get; set; }

        /// <summary>
        /// Gets or sets the Clinical Term.
        /// </summary>
        public string ClinicalTerm { get; set; }

        /// <summary>
        /// Gets or sets the entry datetime.
        /// </summary>
        public string EntryDate
        {
            get
            {
                return this.entryDate;
            }

            set
            {
                if (value != null)
                {
                    if (value.StartsWith("@Today"))
                    {
                        this.entryDate = DateTime.Today.AddDays(Int32.Parse(value.Substring(6))).ToShortDateString();
                    }
                    else
                    {
                        this.entryDate = value;
                    }
                }
                else
                {
                    this.entryDate = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the entry time.
        /// </summary>
        public string EntryTime { get; set; }

        /// <summary>
        /// Gets or sets the ClinicianName.
        /// </summary>
        public string ClinicianName { get; set; }

        /// <summary>
        /// Gets or sets the QuickList menu item.
        /// </summary>
        public string QuickListSelection { get; set; }

        /// <summary>
        /// Gets or sets the Private.
        /// </summary>
        public int Private { get; set; }

        /// <summary>
        /// Gets or sets the In Practice.
        /// </summary>
        public int InPractice { get; set; }

        /// <summary>
        /// Gets or sets the Notes.
        /// </summary>
        public string Notes { get; set; }

    }
}
