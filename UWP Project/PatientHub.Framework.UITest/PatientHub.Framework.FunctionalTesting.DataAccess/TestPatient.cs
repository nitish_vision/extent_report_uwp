﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using SQLite.Net.Attributes;

    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class TestPatient
    {

        /// <summary>
        /// Gets or sets the internal database Id.
        /// </summary>
        [AutoIncrement, PrimaryKey]
        public string ID { get; set; }

        /// <summary>
        /// Gets or sets the family name.
        /// </summary>
        public string FamilyName { get; set; }

        /// <summary>
        /// Gets or sets the given name.
        /// </summary>
        public string GivenName { get; set; }

        /// <summary>
        /// Gets or sets the sequence.
        /// </summary>
        public int Sequence { get; set; }

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Gets or sets the gender.
        /// </summary>
        public string Gender { get; set; }

        /// <summary>
        /// Gets or sets the Identifier - e.g. NHS Number.
        /// </summary>
        public string Identifier { get; set; }

        /// <summary>
        /// Gets or sets the IdentifierType - NhsNumber or ChiNumber.
        /// </summary>
        public string IdentifierType { get; set; }

        /// <summary>
        /// Gets or sets the DateOfBirth
        /// </summary>
        public string DateOfBirth { get; set; }

        /// <summary>
        /// Gets or sets the registered GP.
        /// </summary>
        public string RegisteredGP { get; set; }

        /// <summary>
        /// Gets or sets the usual GP.
        /// </summary>
        public string UsualGP { get; set; }

        /// <summary>
        /// Gets or sets the PracticeID.
        /// </summary>
        public int PracticeID { get; set; }

        /// <summary>
        /// Gets or sets the Practice Name.
        /// </summary>
        public string PracticeName { get; set; }

        /// <summary>
        /// Gets or sets the Short Practice Name.
        /// </summary>
        public string ShortPracticeName { get; set; }

        /// <summary>
        /// Gets or sets the Registration Status.
        /// </summary>
        public string RegistrationStatus { get; set; }

		/// <summary>
        /// Gets or sets the Postcode.
        /// </summary>
        public string Postcode { get; set; }

    }
}
