﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using SQLite.Net.Attributes;

    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class TestBloodPressure : TestEntryBase
    {
        /// <summary>
        /// Gets or sets the Systolic.
        /// </summary>
        public double Systolic { get; set; }

        /// <summary>
        /// Gets or sets the Diastolic.
        /// </summary>
        public double Diastolic { get; set; }

        /// <summary>
        /// Gets or sets the Korotkoff level.
        /// </summary>
        public string Korotkoff { get; set; }

        /// <summary>
        /// Gets or sets the Laterality.
        /// </summary>
        public string Laterality { get; set; }

        /// <summary>
        /// Gets or sets the Posture.
        /// </summary>
        public string Posture { get; set; }

        /// <summary>
        /// Gets or sets the Cuff.
        /// </summary>
        public string Cuff { get; set; }

        /// <summary>
        /// Gets or sets the BodyText (concatenation of Korotkoff, Laterality, Posture, Cuff and Recorded Time used for display.
        /// </summary>
        public string BodyText { get; set; }
    }
}
