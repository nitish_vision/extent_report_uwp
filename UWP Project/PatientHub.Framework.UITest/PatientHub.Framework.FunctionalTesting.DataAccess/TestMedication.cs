﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using SQLite.Net.Attributes;

    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class TestMedication : TestEntryBase
    {
        private string repeatUntilDate;

        /// <summary>
        /// Gets or sets the Search Term.
        /// </summary>
        public string SearchTerm { get; set; }

        /// <summary>
        /// Gets or sets the names of folders to browse.
        /// </summary>
        public string BrowseFolders { get; set; }

        /// <summary>
        /// Gets or sets the Drug Name.
        /// </summary>
        public string DrugName { get; set; }

        /// <summary>
        /// Gets or sets the Gemscript Code.
        /// </summary>
        public double Code { get; set; }

        /// <summary>
        /// Gets or sets the Quantity Default.
        /// </summary>
        public double QuantityDefault { get; set; }

        /// <summary>
        /// Gets or sets the Quantity.
        /// </summary>
        public double Quantity { get; set; }

        /// <summary>
        /// Gets or sets the AvailablePackSizes
        /// </summary>
        public string AvailablePackSizes { get; set; }

        /// <summary>
        /// Gets or sets the PackSize
        /// </summary>
        public double PackSize { get; set; }

        /// <summary>
        /// Gets or sets the default Dosage Instructions.
        /// </summary>
        public string DosageInstructionsDefault { get; set; }

        /// <summary>
        /// Gets or sets the Dosage Instruction.
        /// </summary>
        public string DosageInstructions { get; set; }

        /// <summary>
        /// Gets or sets the StandardDosageDefault
        /// </summary>
        public string StandardDosageDefault { get; set; }

        /// <summary>
        /// Gets or sets the StandardDosage
        /// </summary>
        public string StandardDosage { get; set; }

        /// <summary>
        /// Gets or sets the UserDefinedDosage
        /// </summary>
        public string UserDefinedDosage { get; set; }

        /// <summary>
        /// Gets or sets the default Drug Class.
        /// </summary>
        public string DrugClassDefault { get; set; }

        /// <summary>
        /// Gets or sets the Drug Class.
        /// </summary>
        public string DrugClass { get; set; }

        /// <summary>
        /// Gets or sets the medication type
        /// "Acute Medication", "Repeat Medication", "Issue Medication"
        /// </summary>
        public string MedicationType { get; set; }

        /// <summary>
        /// Gets or sets the number of repeats.
        /// </summary>
        public int NumberOfRepeats { get; set; }

        /// <summary>
        /// Gets or sets the default Preparation.
        /// </summary>
        public string PreparationDefault { get; set; }

        /// <summary>
        /// Gets or sets the Preparation.
        /// </summary>
        public string Preparation { get; set; }

        /// <summary>
        /// Gets or sets the issue number.
        /// </summary>
        public int IssueNumber { get; set; }

        /// <summary>
        /// Gets or sets the total number of issues.
        /// </summary>
        public int TotalIssues { get; set; }

        /// <summary>
        /// Gets or sets the Prescribed Date (Repeats only)
        /// This is the date the repeat was originally prescribed, which may be different from the current repeat date if it has been reauthorised
        /// </summary>
        public string PrescribedDate { get; set; }

        /// <summary>
        /// Gets or sets the Last Issue Date (Repeats only)
        /// </summary>
        public string LastIssueDate { get; set; }

        /// <summary>
        /// Gets or sets the "repeat until" date.
        /// </summary>
        public string RepeatUntilDate
        {
            get
            {
                return this.repeatUntilDate;
            }

            set
            {
                if (value != null)
                {
                    if (value.StartsWith("@Today"))
                    {
                        this.repeatUntilDate = DateTime.Today.AddDays(Int32.Parse(value.Substring(6))).ToShortDateString();
                    }
                    else
                    {
                        this.repeatUntilDate = value;
                    }
                }
                else
                {
                    this.repeatUntilDate = value;
                }
            }
        }

        /// <summary>
        /// Gets or sets the IsGeneric flag.
        /// </summary>
        public int IsGeneric { get; set; }

        /// <summary>
        /// Gets or sets the IsControlled flag.
        /// </summary>
        public int IsControlled { get; set; }

        /// <summary>
        /// Gets or sets the IsHighRisk flag.
        /// </summary>
        public int IsHighRisk { get; set; }

        /// <summary>
        /// Gets or sets the IsDiscontinued flag.
        /// </summary>
        public int IsDiscontinued { get; set; }

        /// <summary>
        /// Gets or sets the EligibleForIssue flag.
        /// </summary>
        public int EligibleForIssue { get; set; }

        /// <summary>
        /// Gets or sets the EligibleForReauth flag.
        /// </summary>
        public int EligibleForReauth { get; set; }

        /// <summary>
        /// Gets or sets the IssueWarnings
        /// </summary>
        public string IssueWarnings { get; set; }

        /// <summary>
        /// Gets or sets the ReauthWarnings
        /// </summary>
        public string ReauthWarnings { get; set; }

        /// <summary>
        /// Gets or sets the LinkedMedications
        /// </summary>
        public List<TestMedication> LinkedMedications { get; set; }

        /// <summary>
        /// Gets or sets the HighRiskWarning
        /// </summary>
        public string HighRiskWarning { get; set; }
    }
}
