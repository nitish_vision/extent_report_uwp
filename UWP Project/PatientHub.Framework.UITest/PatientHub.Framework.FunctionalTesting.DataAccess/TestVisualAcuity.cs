﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using SQLite.Net.Attributes;

    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class TestVisualAcuity : TestEntryBase
    {
        /// <summary>
        /// Gets or sets the Visual Acuity.
        /// </summary>
        public string VisualAcuity { get; set; }

        /// <summary>
        /// Gets or sets the Visual Aid.
        /// </summary>
        public string VisualAid { get; set; }

    }
}
