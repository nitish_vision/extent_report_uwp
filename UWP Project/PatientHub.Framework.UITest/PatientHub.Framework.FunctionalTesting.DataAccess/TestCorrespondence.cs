﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using SQLite.Net.Attributes;

    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class TestCorrespondence : TestEntryBase
    {
        /// <summary>
        /// Gets or sets the Correspondence Type.
        /// </summary>
        public string CorrespondenceType { get; set; }

        /// <summary>
        /// Gets or sets the Headline.
        /// </summary>
        public string DocumentType{ get; set; }

        /// <summary>
        /// Gets or sets the Correspondence Icon.
        /// </summary>
        public string CorrespondenceIcon { get; set; }

        /// <summary>
        /// Gets or sets the Filename.
        /// </summary>
        public string NameOfFile { get; set; }
    }
}
