﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using SQLite.Net.Attributes;

    public class TestImmunisation : TestEntryBase
    {
        /// <summary>
        /// Gets or sets the Search Term.
        /// </summary>
        public string SearchTerm { get; set; }

        /// <summary>
        /// Gets or sets the ImmunisationType.
        /// </summary>
        public string ImmunisationType { get; set; }

        /// <summary>
        /// Gets or sets the Immunisation Stage.
        /// </summary>
        public string Stage { get; set; }

        /// <summary>
        /// Gets or sets the Immunisation Status.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets the Immunisation Stage Value.
        /// </summary>
        public string StageValue { get; set; }

        /// <summary>
        /// Gets or sets the Source.
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        /// Gets or sets the BatchNo.
        /// </summary>
        public string BatchNo { get; set; }

        /// <summary>
        /// Gets or sets the Method.
        /// </summary>
        public string Method { get; set; }

        /// <summary>
        /// Gets or sets the Site.
        /// </summary>
        public string Site { get; set; }

        /// <summary>
        /// Gets or sets the Reason.
        /// </summary>
        public string Reason { get; set; }

        /// <summary>
        /// Gets or sets the ReasonText.
        /// </summary>
        public string ReasonText { get; set; }

        /// <summary>
        /// Gets or sets the DueDate.
        /// </summary>
        public string DueDate { get; set; }
    }
}
