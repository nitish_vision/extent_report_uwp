﻿namespace PatientHub.Framework.FunctionalTesting.Test.DataAccess
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Text.RegularExpressions;
    using System.Threading.Tasks;
    using SQLite.Net.Attributes;

    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class TestAnkleNeuropathy : TestEntryBase
    {
        /// <summary>
        /// Gets or sets the Left Jerk.
        /// </summary>
        public string LeftJerk { get; set; }

        /// <summary>
        /// Gets or sets the Right Jerk.
        /// </summary>
        public string RightJerk { get; set; }

        /// <summary>
        /// Gets or sets the Left Vib.
        /// </summary>
        public string LeftVib { get; set; }

        /// <summary>
        /// Gets or sets the Right Vib.
        /// </summary>
        public string RightVib { get; set; }
    }
}
